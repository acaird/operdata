# detectFromFile.py, Nathan Moos
# This is a test program for detecting anomalies in the number of 
# executed programs per day

from __future__ import print_function
import sys
import json
import datetime
import time
from rpy2 import robjects
from rpy2.robjects import vectors
import rpy2.rlike.container as rlc
from rpy2.robjects.packages import importr
from rpy2.interactive import process_revents
import rpy2.robjects.lib.ggplot2 as ggplot2

AnomalyDetection = importr('AnomalyDetection') 

def json_to_dataframe(jsDat):
    arr = jsDat['facets']['1']['entries']
    timestamps = []
    counts = []
    for o in arr:
        timestamps.append(datetime.datetime.utcfromtimestamp(o['time']/1000))
        counts.append(o['count'])
    ordData = rlc.OrdDict([('time', vectors.POSIXct(timestamps)), ('count', vectors.IntVector(counts))])
    data = robjects.DataFrame(ordData)
    print(data.colnames)
    return data

def plot_detection(data, max_anoms=0.02, direction='both'):
    results = AnomalyDetection.AnomalyDetectionTs(data, max_anoms=max_anoms, direction=direction)
    anoms = robjects.DataFrame(results.rx('anoms')[0])
    anom_pct = float(len(anoms)/len(data))
    print(anoms)
    # plot
    pp = ggplot2.ggplot(data) + \
            ggplot2.aes_string(x='time', y='count') + \
            ggplot2.geom_line(colour='red') + \
            ggplot2.geom_point(ggplot2.aes_string(x='timestamp', y='anoms'), data=anoms, colour='blue', size=8)
    pp.plot()
    
if __name__=='__main__':
    if len(sys.argv) < 2:
        print('usage: python detectFromFile.py <file>')
        sys.exit(1)

    fdesc = open(sys.argv[1])
    jsData = json.load(fdesc)
    plot_detection(json_to_dataframe(jsData), max_anoms=0.20, direction='both')
    while True:
        process_revents.process_revents()
        time.sleep(0.5)

