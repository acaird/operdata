import os
import ldap
import subprocess
import itertools
import logging

class LDAPDirectory (object):
    '''A simple wrapper for LDAP connections that exposes a simplified
    search interface.  At the moment this class only supports anonymous
    binds.'''

    def __init__ (self, uris,
            basedn='',
            scope=ldap.SCOPE_SUBTREE,
            debug=False,
            maxwait=180,
            ):

        self.uris    = itertools.cycle(uris)
        self.maxwait = maxwait

        self.basedn = basedn
        self.scope  = scope
        self.debug  = debug

        self.connect()

    def connect(self):
        uri = self.uris.next()
        logging.info('Connecting to %s' % uri)
        self.dir    = ldap.initialize(uri)

        self.dir.protocol_version = 3
        self.dir.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
        self.dir.set_option(ldap.OPT_X_TLS_DEMAND, True)

        # Set cacert file so we can verify ldap.umich.edu's cert
        ldap.set_option(ldap.OPT_X_TLS_CACERTFILE , 'cacert_ldap_bundle.crt')

        # raises ldap.SERVER_DOWN if reached
        self.dir.set_option(ldap.OPT_NETWORK_TIMEOUT, 30.0)
        # raises ldap.TIMEOUT if reached
        #self.dir.set_option(ldap.OPT_TIMEOUT, 30.0)

        what = ''
        # gets terms, acadprograms, etc.
        who = "cn=CAEN-McDirApp002,ou=Applications,o=services"
        homedir = os.environ['HOME']
        # Get credential from gpg if you have the key...
        #p = subprocess.Popen(['gpg','--decrypt', homedir + '/caen-ldap-creds.gpg'], stdout=subprocess.PIPE)
        # Get credential for McDirApp002 from CAEN, e.g. at a location like
        # /usr/caen/admin/acct/bin/mcauth.out or similar.
        p = subprocess.Popen(['path/to/mcauth_002.out',''], stdout=subprocess.PIPE)
        what, err = p.communicate()
        what = what.strip()

        self.dir.simple_bind_s(who, what)

    def search(self, **kwargs):
        '''Turns kwargs into an LDAP search filter, executes the search,
        and returns the results.  The keys in kwargs are ANDed together;
        only results meeting *all* criteria will be returned.
        
        If the connection to the LDAP server has been lost, search will try
        to reconnect with exponential backoff.  The wait time between
        reconnection attempts will grow no large than self.maxwait.'''

        if not kwargs:
            kwargs = { 'objectclass': '*' }

        filter = self.build_filter(**kwargs)


        res = self.dir.search_s(
                self.basedn,
                self.scope,
                filterstr=filter,
                attrlist=None, # or ['member'], e.g.
                attrsonly=0) 
        return res

    def build_filter(self, **kwargs):
        '''Transform a dictionary into an LDAP search filter.'''

        filter = []
        for k,v in sorted(kwargs.items(), key=lambda x: x[0]):
            filter.append('(%s=%s)' % (k,v))

        if len(filter) > 1:
            return '(&%s)' % ''.join(filter)
        else:
            return filter[0]

