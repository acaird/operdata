#!/usr/bin/python
import ldap
import csv
import os

#######################################################################################

def user_search(l, user):
        try:
                result_id = l.search("", ldap.SCOPE_SUBTREE, "uid="+user, None)
                result_type, result_data = l.result(result_id, 0)
                if(len(result_data) > 0):
# Ann Arbor Student?
                        umichAACurrentTermStatus = {}
                        umichAAAcadProgram = {}
                        try:
                                for x in result_data[0][1]['umichAACurrentTermStatus'][0].split(":"):
                                        y, z = x.strip("\'\{\}").split("=")
                                        umichAACurrentTermStatus[y] = z
                                for uAP in result_data[0][1]['umichAAAcadProgram']:
                                        w = result_data[0][1]['umichAAAcadProgram'].index(uAP)
                                        umichAAAcadProgram[w] = {}
                                        for x in uAP.split(":"):
                                                y, z = x.strip("\'\{\}").split("=")
                                                umichAAAcadProgram[w][y] = z
                        except:
                                role = ""
# Faculty or Staff
                        umichHR = {}
                        try:
                                for uHR in result_data[0][1]['umichHR']:
                                        w = result_data[0][1]['umichHR'].index(uHR)
                                        umichHR[w] = {}
                                        for x in uHR.split(":"):
                                                y, z = x.strip("\'\{\}").split("=")
                                                umichHR[w][y] = z
                        except:
                                role = ""
# Student Block
                        if umichAACurrentTermStatus:
                                role = "StudentAA"
                                roledesc = umichAACurrentTermStatus['acadLevelDescription']
                                college =  umichAACurrentTermStatus['acadCareerDescr']
                                try:
                                        department1 = umichAAAcadProgram[0]['acadPlanFieldDescr']
                                except:
                                        department1 = ""
                                try:
                                        department2 = umichAAAcadProgram[1]['acadPlanFieldDescr']
                                except:
                                        department2 = ""
# Staff Faculty Block
                        elif umichHR:
                                try:
                                        role = umichHR[0]['jobCategory']
                                except:
                                        role = ""
                                try:
                                        roledesc = result_data[0][1]['umichTitle'][0]
                                except:
                                        roledesc = ""
                                try:
                                        college = umichHR[0]['deptGroupDescription']
                                except:
                                        college = ""
                                try:
                                        department1 = umichHR[0]['deptDescription']
                                except:
                                        department1 = ""
                                try:
                                        department2 = umichHR[1]['deptDescription']
                                except:
                                        department2 = ""
# Others Block
                        else:
                                try:
                                        for x in result_data[0][1]['umichInstRoles']:
                                                if ("Student" in str(x)):
                                                        role = str(x)
                                                        break
                                                if ("SponsoredAffiliate" in str(x)):
                                                        role = str(x)
                                                        break
                                                role = str(x)
                                except:
                                        role = ""
                                roledesc = ""
                                college = ""
                                department1 = ""
                                department2 = ""

                        row = [user, role, roledesc, college, department1, department2]
                        return row
# Non-LDAP
                else:
                        role = "No Entry"
                        roledesc = ""
                        college = ""
                        department1 = ""
                        department2 = ""
                        row = [user, role, roledesc, college, department1, department2]
                        return row
        except ldap.LDAPError, e:
                print e

#######################################################################################

def main():
        list = set([])
        row = []

#        rdir = "/home/openit/openit_html_data/results/{Temporary}"
        rdir = "D:/Data/openit_html_data/results/{Temporary}/"
        os.chdir(rdir)
        rfile = filter(lambda x: x.startswith('top_1329247721_29804_') and x.endswith('.csv'), os.listdir(rdir))
        for file in rfile:
                ulfile = csv.reader(open(file, 'rb'))
                print "Reading %s" % file
                for row in ulfile:
                        list.add(row[0])

        cfile = open('D:/users.csv', 'wb')
        c = csv.writer(cfile, delimiter=";", quoting=csv.QUOTE_NONE)

        cheader = ['#UserName', 'CostCenter', 'CostCenterDescription', 'Location', 'Department', 'Division', '']
        c.writerow(cheader)

        server = "ldaps://ldap.umich.edu:636"
        baseDN = "cn=xxxxx,ou=Applications,o=services"
        password = "xxxxx"
        try:
                ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
                print "Opening LDAP connection"
                l = ldap.initialize(server)

#see http://stackoverflow.com/questions/7716562/pythonldapssl
                l.set_option(ldap.OPT_REFERRALS, 0)
                l.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
#                l.set_option(ldap.OPT_X_TLS,ldap.OPT_X_TLS_DEMAND)
#                l.set_option(ldap.OPT_X_TLS_DEMAND, True )
                l.set_option(ldap.OPT_DEBUG_LEVEL, 255 )

                print "Connection opened"
                l.simple_bind_s(baseDN, password)
                print "Connection bound"
                print "Generating file..."
                for user in list:
                        row = user_search(l, user)
                        c.writerow(row)
                print "Finished"
        except ldap.LDAPError, e:
                print e

#######################################################################################

if __name__=='__main__':
    main()
