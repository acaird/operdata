import ldapdirectory
import re
# Really simple uniqname-caching mechanism

class UniqnameFetcher:
    def __init__(self, directory):
        self.directory = directory
        self.queries = {}

    def lookup(self, uniqname):
        if uniqname in self.queries:
            return self.queries[uniqname]

        res = self.directory.search(uid=uniqname)
        if len(res) > 0:
            # Unwrap
            res = res[0][1]

            # Clean up the data (directly from Tom's code
            if 'umichHR' in res:
                umHR = res['umichHR'][0]
                if umHR is not None:
                    cleanHR = re.sub('[{}]', '', umHR)
                    cleanHR = re.sub(': ', ' - ', cleanHR)
                    res['umichHR'] = dict([x.split('=') for x in cleanHR.split(':')])

            if 'umichAATermStatus' in res:
                umTS = res['umichAATermStatus']
                newTS = []
                for item in umTS:
                    cleanTS = re.sub('[{}]', '', item)
                    keyedTS = dict([x.split('=') for x in cleanTS.split(':')])
                    newTS.append(keyedTS)
                res['umichAATermStatus'] = newTS

            if 'umichAACurrentTermStatus' in res:
                umCTS = res['umichAACurrentTermStatus']
                newCTS = []
                for item in umCTS:
                    cleanCTS = re.sub('[{}]', '', item)
                    keyedCTS = dict([x.split('=') for x in cleanCTS.split(':')])
                    newCTS.append(keyedCTS)
                res['umichAACurrentTermStatus'] = newCTS

            # split up umichAAAcadProgram if it exists
            if 'umichAAAcadProgram' in res:
                 umAP = res['umichAAAcadProgram'] # list
                 newAP = []
                 for item in umAP:
                    cleanAP = re.sub('[{}]', '', item)
                    # brute - try to get them all
                    cleanerAP = re.sub(': ', ' - ', cleanAP)
                    # still more.  e.g. dasdad
                    cleanerAP = re.sub('acadProgDescr=Performance:', 'acadProgDescr=Performance - ', cleanerAP)
                    cleanerAP = re.sub('acadProgDescr=Music Performance:', 'acadProgDescr=Music Performance - ', cleanerAP)
                    cleanerAP = re.sub('acadProgDescr=Music Perform:', 'acadProgDescr=Music Performance - ', cleanerAP)
                    keyedAP = dict([x.split('=') for x in cleanerAP.split(":")])
                    newAP.append(keyedAP)
                 res["umichAAAcadProgram"] = newAP

            # split up umichAlumStatus if it exists
            if 'umichAlumStatus' in res:
                 umAStat = res['umichAlumStatus'] # list
                 newAStat = []
                 for item in umAStat:
                    cleanAStat = re.sub('[{}]', '', item)
                    keyedAStat = dict([x.split('=') for x in cleanAStat.split(":")])
                    newAStat.append(keyedAStat)
                 res["umichAlumStatus"] = newAStat

            # note different delimiters and patterns!
            # split up umichServiceEntitlement if it exists
            if 'umichServiceEntitlement' in res:
                 umSE = res['umichServiceEntitlement'] # list
                 newSE = []
                 for item in umSE:
                    cleanSE = re.sub('[{}\"]', '', item)
                    keyedSE = dict([x.split(':') for x in cleanSE.split(",")])
                    newSE.append(keyedSE)
                 res['umichServiceEntitlement'] = newSE

            self.queries[uniqname] = res
            return res
        return None

    def clear_cache(self):
        self.queries = {}

