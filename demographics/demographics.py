import exceptions
import uniqname
from pprint import pprint as pp

# This will need to be changed when run outside the 40-net ring
HR_INFO = (
        u'deptGroup',
        u'deptGroupDescription',
        u'deptId',
        u'deptDescription',
        u'jobCategory',
        u'jobFamily',
        u'jobcode',
        u'supervisorId'
        )

AAACAD_PROGRAM_INFO = (
        u'acadCareer',
        u'acadCareerDescr',
        u'acadPlanDegree',
        u'acadPlanDescr',
        u'acadPlanFieldDescr',
        u'acadProgDescr',
        u'acadPlanType'
        )

def acad_plan_priority(acadprogram):
    '''
    Defines a sort order for academic programs.
    This makes it possible to simply sort the academic program info into the
    following order when a user has multiple programs:
    -> Major-Major
    -> Major-Minor
    -> Major-Specialization
    -> Major
    '''
    if acadprogram[u'acadPlanType'] == 'MAJ':
        return 0
    elif acadprogram[u'acadPlanType'] == 'MIN':
        return 1
    else:
        return 2

def subset_dict(d, keys, keyTransform=None):
    '''
    This takes a subset of a dictionary corresponding to the keys.
    '''
    if keyTransform:
        return dict((keyTransform(k), d[k]) for k in keys if k in d)
    else:
        return dict((k, d[k]) for k in keys if k in d)

def get_demographics(uniqname, fetcher):
    '''
    Fetches demographic information for the user specified.
    '''
    dems = {}
    # Fetch
    content = fetcher.lookup(uniqname)
    if content is None:
        return {}

    # Process
    if u'umichTitle' in content:
        dems[u'umichTitle'] = content[u'umichTitle'][0]
    try:
        dems[u'umichInstRoles'] = content[u'umichInstRoles']
    except KeyError:
        print "Missing umichInstRoles."
        #pp(content)
        print uniqname
        #raise KeyError('umichInstRoles')
        with open("missing_roles.txt", "a") as myfile:
            myfile.write(uniqname + "\n")
    if u'umichHR' in content:
        jobs = content[u'umichHR']
        if type(jobs) is dict:
            job = jobs
        elif type(jobs) is list and len(jobs) > 0:
            job = jobs[0]
        if job:
            dems.update(subset_dict(job, HR_INFO))

    if u'umichAAAcadProgram' in content:
        academicProgram = content[u'umichAAAcadProgram']
        if len(academicProgram) == 1:
            # Only one academic program here.
            firstProg = academicProgram[0]
            dems.update(subset_dict(firstProg, AAACAD_PROGRAM_INFO, lambda k: k + '1'))
        elif len(academicProgram) > 1:
            # We have multiple academic programs, sort them
            acadProgramSorted = sorted(academicProgram, key=acad_plan_priority)
            firstProg = acadProgramSorted[0]
            dems.update(subset_dict(firstProg, AAACAD_PROGRAM_INFO, lambda k: k + '1'))
            secondProg = acadProgramSorted[1]
            dems.update(subset_dict(secondProg, AAACAD_PROGRAM_INFO, lambda k: k + '2'))


    if u'umichAACurrentTermStatus' in content:
        currentTermStatus = content[u'umichAACurrentTermStatus']
        if len(currentTermStatus) > 0:
            currentTermStatus = currentTermStatus[0]
            dems[u'acadLevelDescription'] = currentTermStatus[u'acadLevelDescription']

    return dems

