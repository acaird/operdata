import sys
import demographics
import ldapdirectory
import uniqname
from elasticsearch import Elasticsearch
import elasticsearch.helpers as ESHelpers
from pprint import pprint as pp

import time
import datetime

LDAP_URIS = ['ldaps://ldap.umich.edu']
ES_HOSTS = ['localhost','hpc-es.engin.umich.edu']

def main(argv):

    start = time.time()

    #make sure the only thing on the command line is the name of an index
    if(len(sys.argv) != 2):
        print 'ERROR: only specify the name of the index on command line'
        exit(1)

    #connect to the ES instance running on the machine
    es = Elasticsearch(host=ES_HOSTS[0])

    #grab the name of the index from the command line
    index = argv[1]

    #log into LDAP
    directory = ldapdirectory.LDAPDirectory(LDAP_URIS)
    fetcher = uniqname.UniqnameFetcher(directory)

    #make a count query to the index specified on the command line and save result to
    #documents_of_interest.  this allows us to grab the number of docs to iterate on
    documents_of_interest = es.count(index=index, q='_exists_:username AND NOT _exists_:umichInstRoles')

    #gives us the total number of shards in the index for looping purposes
    loop_size = documents_of_interest['count']
    print '\nFound %s documents of interest in index %s!\n' % (loop_size,index)

    #get the full collection of documents, now that we have a count
    #doc_collection = es.search(index=index, size=loop_size, q='_exists_:username')
    doc_collection = ESHelpers.scan(es, scroll='30m', index=index, q='_exists_:username AND NOT _exists_:umichInstRoles')

    #print '\nRetrieved collection of documents!\n'

    troubled_souls = set([]) # empty set to hold usernames that are not uniqnames
    reindexed_count = 0
    skipped_count = 0

    for i,item in enumerate(doc_collection):
        #Grab the "important piece" of the current record
        #item = doc_collection['hits']['hits'][i] 

        #grab the doc id to be used in index and get methods
        id_num = item['_id']
        old_type = item['_type']

        #if the doc_type is winevent, change it to windows and add a new tag
        if item['_type'] == 'winevent':
            item['_type'] = 'windows'
            #what should this tag be called to store the old winevent type
            item['_source']['tags'] += ['winevent']
            item['_source']['version'] = '2014'
            item['_source']['product'] = 'Win7-CLSE'
            if 'edition' in item['_source']:
                if item['_source']['edition'] == None:
                    item['_source']['edition'] = 'unknown'
                else:
                    item['_source']['edition'] = item['_source']['edition'].lower()
            else :
                item['_source']['edition'] = 'unknown'

        elif item['_type'] == 'linux':
            item['_source']['version'] = '2014'
            item['_source']['product'] = 'RHEL6.5-CLSE'
            if 'edition' in item['_source']:
                if item['_source']['edition'] == None:
                    item['_source']['edition'] = 'unknown'
                else:
                    item['_source']['edition'] = item['_source']['edition'].lower()
            else :
                item['_source']['edition'] = 'unknown'

        #if there is a field called accountname, add one called username with
        #same val
        if 'accountname' in item['_source']:
            item['_source']['username'] = item['_source']['accountname'].lower()

        #if there is no shorthost in doc, create one
        if 'shorthost' not in item['_source']:
            #shorthost, extra = item['_source']['host'].split('.', 1)
            shorthost = item['_source']['host'].split('.', 1)
            shorthost[0].lower()
            item['_source']['shorthost'] = shorthost

        #if host field exists, lowercase it
        if 'host' in item['_source']:
            item['_source']['host'].lower()

        #also need to grab the doc_type from the current record
        doc_type = item['_type']

        # Add demographic information to the entries
        name = item['_source']['username']

        # only if we have not already checked for this uniqname
        if name not in troubled_souls:
            update_dict = demographics.get_demographics(name, fetcher)
            item['_source'].update(update_dict)

        if 'umichInstRoles' not in item['_source']:
            skipped_count += 1
            # so such luck with this uniqname
            troubled_souls.add(name)
            record = "Nothing to update."
        else:
            # reindex with update, possibly changing es type
            reindexed_count += 1
            record = es.index(index=index, doc_type=doc_type, id=id_num, body=item['_source'])
            if u'created' in record:
               if record[u'created']:
                    # here, we will delete the old one
                    es.delete(index=index, doc_type=old_type, id=id_num)

        # print something to indicate progress.  collections can be large!
        if i % 1000 == 0:
            now = time.time()
            delta = now - start
            print '\nOutput from es.index():'
            pp(record)
            print 'Completed: {0} of {1}\n'.format(i+1,loop_size)
            print 'Reindexed: %s' % (reindexed_count)
            print 'Skipped: %s' % (skipped_count)
            print 'Elapsed: %s' % (str(datetime.timedelta(seconds=delta)))
            #pp(es.get(index=index, id=id_num))
            #pp(troubled_souls)

    print('All done!\n')

    odd_ducks = sorted(troubled_souls)
    pp(odd_ducks)

    print 'Records: %s' % (loop_size)
    print 'Reindexed: %s' % (reindexed_count)
    print 'Skipped: %s' % (skipped_count)

if __name__ == '__main__':
    main(sys.argv)

#logstash-caen-labs-xx-2014.09.01 (01-09) == testing indices    
