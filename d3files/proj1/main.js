define(['scripts/d3.v3', 'scripts/elasticsearch'], function(d3, elasticsearch) {
	var client = new elasticsearch.Client();

	client.search({
		index: 'flux-software-2014.07',
		size: 10,
		body: {
			aggs: {
				commusers: {
					terms: {
						field: "comm",
						order: { "user_count" : "desc" }
					},
					aggs: {
						user_count: {
							cardinality: {
								field: "username"
							}
						}
					}
				}
			}
		}
	}).then(function(resp) {
		console.log(resp);

		var commusers = resp.aggregations.commusers.buckets;

		var width = 1500,
			height = 1500,
			radius = Math.min(width, height) / 2;

		var color = ['#aec7e8', '#98df8a', '#ff9896', '#c5b0d5',
			 '#9edae5', '#b5cf6b', '#fdae6b', '#de9ed6', '#9c9ede', '#d6616b'];

		//define arc size
		var arc = d3.svg.arc()
			.outerRadius(radius - 30)
			.innerRadius(0);

		//define pie slices
		var pie = d3.layout.pie()
			.sort(null)
			.value(function (d) { return d.user_count.value; });

		// create svg window
		var svg = d3.select("#piechart").append("svg")
			.attr("width", width)
			.attr("height", height)
			.append("g")
			.attr("transform", "translate(" + width/2 + "," + height/2 + ")" );

		var g = svg.selectAll(".arc")
			.data(pie(commusers))
			.enter()
			.append("g")
			.attr("class", "arc");

		g.append("path")
			.attr("d", arc)
			.style("fill", function (d, i) { return color[i]; });


		g.append("text")
			.attr("transform", function (d) { return "translate(" + arc.centroid(d) + ")"; })
			.attr("dy", ".35em")
			.style("text-anchor", "middle")
			.text(function (d) {return d.data.key;});

	});
});