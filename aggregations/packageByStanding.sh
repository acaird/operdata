# aggregation to order package by unique user
# ignores chrome.exe and net.exe
# logstash-caen-labs-v1-2014.12.12,logstash-caen-labs-v1-2014.12.11
curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-2014.12.*,,logstash-caen-labs-v1-2014.11.*,logstash-caen-labs-v1-2014.10.*,logstash-caen-labs-v1-2014.09.*/_search?pretty' -d '{
"query": {
"filtered": {
 "filter": {
   "bool": {
     "must_not": [
       {
         "terms": {
           "username": [
             "root"
           ]
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username: *1*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username:*2*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username: *0*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username:*3*"
             }
           },
           "_cache": true
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\TortoiseSVN\\bin\\TSVNCache.exe",
             "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe",
             "C:\\Program Files (x86)\\Google\\Chrome\\Application\\39.0.2171.65\\Installer\\setup.exe",
             "C:\\Program Files\\Microsoft Office\\Office15\\msoia.exe",
             "C:\\Program Files (x86)\\Adobe\\Reader 11.0\\Reader\\AcroRd32.exe",
             "C:\\Program Files\\SolidWorks Corp\\SolidWorks\\sldShellExtServer.exe",
             "C:\\Python27\\ArcGIS10.2\\python.exe",
             "C:\\Program Files (x86)\\Common Files\\Adobe\\CS6ServiceManager\\CS6ServiceManager.exe",
             "C:\\Program Files\\Heimdal\\bin\\krbcc64s.exe",
             "C:\\Program Files (x86)\\Google\\Chrome\\Application\\38.0.2125.111\\Installer\\chrmstp.exe",
             "C:\\Program Files (x86)\\Google\\Chrome\\Application\\37.0.2062.124\\nacl64.exe",
             "C:\\Program Files (x86)\\Google\\Chrome\\Application\\38.0.2125.111\\nacl64.exe",
             "C:\\Program Files (x86)\\Adobe\\Acrobat 11.0\\Acrobat\\acrotray.exe",
             "C:\\Program Files\\Microsoft Security Client\\msseces.exe",
             "C:\\Program Files\\OpenAFS\\Client\\Program\\afscreds.exe",
             "C:\\Program Files (x86)\\Box\\Box Edit\\Box Edit.exe",
             "C:\\Program Files (x86)\\PaperCut MF Client\\pc-client.exe",
             "C:\\Program Files (x86)\\PaperCut MF Client\\pc-client.exe",
             "C:\\Program Files\\Microsoft Office\\Office15\\msoia.exe",
             "C:\\Program Files (x86)\\Adobe\\Acrobat 11.0\\Acrobat\\acrodist.exe",
             "C:\\Program Files\\NVIDIA Corporation\\nView\\nwiz.exe",
             "C:\\Program Files\\NVIDIA Corporation\\Display\\nvtray.exe",
             "C:\\Program Files (x86)\\National Instruments\\NI-DAQ\\HWConfig\\nidevmon.exe",
             "C:\\Program Files\\TortoiseSVN\\bin\\TSVNCache.exe",
             "C:\\Program Files\\Realtek\\Audio\\HDA\\RAVCpl64.exe"
           ]
         }
       }
     ]
   }
 }
 }
 },
"aggs": {
	"department": {
		      "terms": {
		      	       "field" : "acadPlanFieldDescr1.raw",
			       "size": 25
			       },
			"aggs": {
				"classStanding": {
						  "terms": {
						  	 "field": "acadLevelDescription.raw"
							   },
				  "aggs": {
				  	  "swExe": {
					 	  "terms" : {
						    	  "field" :  "exe.raw",
                                                          "size"  : 5
						  	  },
								"aggs": {
									"users": {
										"terms" : {
											"field" : "username",
											"size"  : 5
										},
							"aggs": {
								"user_count": {
									"cardinality": {
										"field": "username"
										}
									}
	 			  	  			}
							}
						}
					}
	 		          	}
				}
			  }
		}
	}
}'
