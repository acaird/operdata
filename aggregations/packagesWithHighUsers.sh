curl -XGET 'http://localhost:9200/flux-software-*/_search?pretty' -d '{
	"aggs": {
		"comm_users": {
			"terms": {
				"field": "comm",
				"size": 10,
				"order": {
					"user_count" : "desc"
				}
			},
			"aggs": {
				"user_count": {
					"cardinality": {
						"field": "username"
					}
				}
			}
		}
	}
}'
