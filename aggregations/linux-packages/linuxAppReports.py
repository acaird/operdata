#   -*- mode: python; indent: 4 -*-
import sys
import json
import datetime
# pip install plotly
import plotly.plotly as py
from plotly.graph_objs import *

"""

"""

def getJsonFromElasticSearch (url,arg):
    """
    Contact the Elasticsearch server, load the json into a dictionary
    """
    import urllib
    import urllib2
    jstring = urllib2.urlopen(url, arg).read()
    return(json.loads(jstring)['aggregations']['comm_users']['buckets'])

def esSearch (url, urlpath, query):
    """
    Build the URL path and send it and the query to get the data;
    this is separate so that the data can be read from other
    places (like a file) if needed.

    Once the data is here, make a smaller dictionary with more
    useful keys and some embedded formatting for text output
    """
    fullurl = url+urlpath
    jdata = getJsonFromElasticSearch(fullurl, query)
    sw = {}
    swlist = []
    string = []
    for m in jdata:
        # if m['key'] == 'mpirun':
        #         continue
        # if m['key'] == 'orted':
        #         continue
        title = '{:>20s}'.format(m['key'].encode('ascii',errors='ignore'))
        if not sw.has_key(title):
            sw[title] = {}
        sw[title]['users']  = '{:6d}'.format(m['user_count']['value'])
        sw[title]['starts'] = '{:8d}'.format(m['doc_count'])
        sw[title]['group'] = ""

    return(sw)

def software(os, console=None):
    """
    Build up the JSON query (Tom has a better way that will
    hopefully replace this), get the results, convert it to some
    lists (because the Better Way was beyond me at the moment),
    and send it all to the render_template to make some HTML
    """

    url     = 'http://localhost:9200/'
    urlpath = 'logstash-caen-labs-v1-*/_search'

    myarg='''
     {
      "query" : {
        "filtered" : {
          "filter": {
              "bool": {
                  "must": [
                      {
                          "terms": {
                              "_type": [
        '''

    myarg = myarg + '"linux"'

    myarg = myarg + '''
                              ]
                          }
                      }
                  ]
              }
          }
        }
      },
     "aggregations": {
       "comm_users": {
         "terms": { '''

    myarg = myarg + '"field":' + '"package.raw"'  + ','

    myarg = myarg + '''
          "size": 0
         },
       "aggregations": {
       "user_count": {
         "cardinality": {
           "field": "username"      }     }     }     }     }
     }'''

    swlist = esSearch(url,urlpath,myarg)
    today = str(datetime.date.today())
    csv = []
    for m in (sorted(swlist,key=lambda x:swlist[x]['users'],reverse=True)):
        csv.append(m+","+swlist[m]['users']+","+swlist[m]['starts']+"\n")

    return(csv)

def getPackageStats (csv,pkgs):
    ranList = {}
    for m in csv:
        m = m.strip()
        title = m.split(",")[0]
        users = int(m.split(",")[1])
        starts = int(m.split(",")[2])
        ranList[title] = users

    pkg={}
    for m in pkgs:
        m = m.strip()
        pkg[m]=0
        if m in ranList:
            pkg[m]=ranList[m]

    return(pkg)

def bin_data(pkg):
    bins={}
    mostUsers = 51
    incr = 1
    for number in range(0,mostUsers,incr):
        if (number not in bins):
            bins[number] = 0
        for p in pkg:
 #           if (number-incr) < pkg[p] <= number:   # to bin ranges
            if pkg[p] <= number:                  # to count sums
                bins[number] = bins[number] + 1
    return(bins)

def console_output(os,pkgs,numRuns):

    csv = software(os,"csv")
    pkg = getPackageStats(csv,pkgs)

    print '{0:40s} {1:12s}'.format("Package Title", "Unique Users")
    print '{0:40s} {1:12s}'.format("==============================", "============")
    for m in sorted(pkg, key=lambda x: pkg[x]):
        if pkg[m] <= int(numRuns):
            print '{0:40s} {1:4d}'.format(m, pkg[m])

def plotly_output(os, pkgs):
    csv = software(os,"csv")
    pkg = getPackageStats(csv,pkgs)
    bins = bin_data(pkg)

    py.sign_in("acaird", "2ufmalengi")

    trace1 = Scatter(
        x= bins.keys(),
        y= bins.values(),
        fill='tozeroy',
        fillcolor="red",
        opacity=0.52,
        name="Unused Packages"
        )
    trace2 = Scatter(
        x= bins.keys(),
        y= [len(pkg)] * len(bins),
        fill='tonexty',
        fillcolor="green",
        opacity=0.52,
        name="Total Installed Packages"
        )
    layout = Layout(
        title='Number of CAEN Linux Packages versus Unique Users',
        xaxis=XAxis(
            title='Unique Users Cut-off',
            titlefont=Font(
                size=18,
                color='#7f7f7f'
            )
        ),
        yaxis=YAxis(
            title='Number of Packages',
            titlefont=Font(
                size=18,
                color='#7f7f7f'
            )
        )
    )
    data = Data([trace1,trace2])
    fig = Figure(data=data, layout=layout)
    plot_url = py.plot(fig, filename='linux-sw-packages', auto_open=False)
    iframe_string = '<iframe id="igraph" style="border:none" src="'+plot_url+'/550/550" width="100%" height="700"></iframe>'
    print "Plot is at: ",plot_url, "(iframe:",iframe_string,")"

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--os", action="store", dest="opsys",
                        choices=['linux', 'windows'], const='windows',
                        help="chose an operating system on which to "+
                        "report software usage; implies console output "+
                        "and defaults to 'windows'",
                        nargs='?')
    parser.add_argument("-n", "--number-of-launches", action="store", dest="numRuns",
                        help="show applications run NUMRUNS times or fewer", default=0)
    parser.add_argument("-p", "--plot", action="store_true", dest="plot",
                        help="make a plot at plot.ly", default=0)

    args = parser.parse_args()
    file = open('linux-packages.txt', "r")
    packages = file.readlines()
    if (args.opsys and not args.plot):
        console_output(args.opsys, packages, args.numRuns)
    if (args.opsys and args.plot):
        plotly_output(args.opsys, packages)
