# orders packages by number of unique users
# prints top 5 unique users per package
# prints last five time stamps per package with username associated with timestamp

curl -XGET 'http://localhost:9200/flux-software-*/_search?pretty' -d '{
	"aggs": {
		"times": {
			"terms": {
				"field": "comm",
				"size": 10,
				"order": { "top_users" : "desc" }
			},
			"aggs": {
				"top_users": {
					"cardinality": {
						"field": "username"
					}
				},
				"usernames": {
					"terms": {
						"field": "username",
						"size": 5
					}
				},
				"timestamp": {
					"terms": {
						"field": "@timestamp",
						"size": 5
					},
					"aggs": {
						"users": {
							"terms": {
								"field": "username"
							}
						}
					}
				}
			}
		}
	}
}'