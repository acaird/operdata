curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-2015.01.16,logstash-caen-labs-v1-2015.01.15,logstash-caen-labs-v1-2015.01.14,logstash-caen-labs-v1-2015.01.13,logstash-caen-labs-v1-2015.01.12,logstash-caen-labs-v1-2015.01.11,logstash-caen-labs-v1-2015.01.10,logstash-caen-labs-v1-2015.01.09,logstash-caen-labs-v1-2015.01.08,logstash-caen-labs-v1-2015.01.07,logstash-caen-labs-v1-2015.01.06,logstash-caen-labs-v1-2015.01.05,logstash-caen-labs-v1-2015.01.04,logstash-caen-labs-v1-2015.01.03,logstash-caen-labs-v1-2015.01.02/_search?pretty' -d '{

"query": {
"filtered": {
 "filter": {
   "bool": {
    "must": [
      {
       "range": {
         "@timestamp": {
           "from": "now-1w",
           "to": "now"
	}
       }
      }
     ],
     "must_not": [
       {
         "terms": {
           "username": [
             "root"
           ]
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username: *0*",
               "query": "username: *1*",
               "query": "username: *2*",
               "query": "username: *3*"
             }
           },
           "_cache": true
         }
       }
     ]
   }
 }
 }
 },
  "aggs": {
     "building": {
         "terms": {
	   "field":"building"
         },
	 "aggs": {
	   "room": {
	     "terms":{
	       "field":"room"
             },
	     "aggs": {
	       "computer": {
	         "terms":{
		   "field":"shorthost.raw"
	         }
	       }
             }
           }
         }
     }
  },
  "size": 0
}'
