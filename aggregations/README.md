Aggregation Descriptions:

-   exeByLogonID.sh

orders the exe by the greatest amount of logonids, then prints the
uniqname with the greatest amount of logonids per exe

-   exeByUniqname.sh

orders the exe by greatest number of unique users, ignores chrome.exe
and net.exe

-   packageByStanding.sh

report department, grade level, top 5 executables, and top 5 users for
each of those; this excludes things we are currently considering noise

-   packageByTimeStamp.sh

orders packages by number of unique users and prints the last five time
stamps per package with the username associated with the given timestamp

-   packageWithHighUsers.sh

orders packages by the greatest amount of unique users

-   packagesWithLowUsers.sh

orders packages by the least amount of unique users

titlesBySession.sh
orders out the titles by the highest number of sessions with filtering unneccesary titles

titlesWithStudentInfo.sh
orders the packages by most unique users, filters out unneccesary titles, then prints out the top five unique users for each with their class standing and major

topExeByWeek.sh
orders the executable by the greatest amount of unique users, then displays the amount of unique users each exe had per week
=======
-   research.sh

prints executables from research editions, ordered by launches with no
exclusions

-   topExeByWeek.sh

orders the executable by the greatest amount of unique users, then
displays the amount of unique users each exe had per week

-   topPackagesWithUsers.sh

displays the packages with the greatest amount of unique users and its
five top users per package

-   topPackagesWithUsersAndTimes.sh

orders packages by number of unique users, prints the top five unique
users per package, and prints the last five time stamps of the package
with the username associated with the timestamp

-   topUsersWithHosts.sh

orders users by the greatest amount of instances with their corresponding hosts & instances

exclusions.txt
includes a filter that can be placed at the beginning of any aggregation to filter out unnecessary titles
