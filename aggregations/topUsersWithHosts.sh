
# aggregation to print usernames with most doc counts
# and their corresponding hosts with doc counts


curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty' -d '{
	"aggs": {
		"users": {
			"terms": {
				"field": "username"
			},
			"aggs": {
				"hosts": {
					"terms": {
						"field": "host"
					}
				}
			}
		}
	}
}'