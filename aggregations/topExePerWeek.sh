# aggregation to order package by unique user

curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty' -d '{
	"aggs": {
		"exe": {
			"terms": {
				"field": "exe.raw",
				"order": {
					"user_count": "desc"
				}
			},
			"aggs": {
				"user_count": {
					"cardinality": {
						"field": "username"
					}
				},
				"week_range": {
					"date_histogram": {
						"field": "@timestamp",
						"interval": "week",
						"min_doc_count": 0
					},
					"aggs": {
						"users_per_week": {
							"cardinality": {
								"field": "username"
							}
						}
					}
				}
			}
		}
	}
}'
