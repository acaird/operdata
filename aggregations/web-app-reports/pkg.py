import json
import collections
import re
from elasticSearch import getJsonFromElasticSearch
import exclusions
from pprint import pprint

def pkgByUserByStandingByDepartmentParse (data):

    '''
    Wow is this a mess.  We might as well just hardcode the data.  -acaird
    '''
    p = collections.OrderedDict()
    for numDepts in range(0,len((data['department']['buckets']))):
        dept = data['department']['buckets'][numDepts]['key']
        deptLaunches = data['department']['buckets'][numDepts]['doc_count']
        p[dept] = collections.OrderedDict()
        p[dept]['launches'] = deptLaunches
        for numStanding in range(0,len(data['department']['buckets'][numDepts]['classStanding']['buckets'])):
            standing = data['department']['buckets'][numDepts]['classStanding']['buckets'][numStanding]['key']
            standingLaunches = data['department']['buckets'][numDepts]['classStanding']['buckets'][numStanding]['doc_count']
            p[dept][standing] = collections.OrderedDict()
            p[dept][standing]['launches'] = standingLaunches
            p[dept][standing]['launchFraction'] = standingLaunches/float(p[dept]['launches'])
            for numExes in range(0,len(data['department']['buckets'][numDepts]['classStanding']['buckets'][numStanding]['swExe']['buckets'])):
                swPackage = data['department']['buckets'][numDepts]['classStanding']['buckets'][numStanding]['swExe']['buckets'][numExes]['key']
                swLaunches= data['department']['buckets'][numDepts]['classStanding']['buckets'][numStanding]['swExe']['buckets'][numExes]['doc_count']
                p[dept][standing][swPackage] = collections.OrderedDict()
                p[dept][standing][swPackage]['launches'] = swLaunches
                p[dept][standing][swPackage]['launchFraction'] = swLaunches/float(p[dept][standing]['launches'])
                for numUsers in range(0, len((data['department']['buckets'][numDepts]['classStanding']['buckets'][numStanding]['swExe']['buckets'][numExes]['users']['buckets']))):
                    user = data['department']['buckets'][numDepts]['classStanding']['buckets'][numStanding]['swExe']['buckets'][numExes]['users']['buckets'][numUsers]['key']
                    userLaunches = data['department']['buckets'][numDepts]['classStanding']['buckets'][numStanding]['swExe']['buckets'][numExes]['users']['buckets'][numUsers]['doc_count']
                    p[dept][standing][swPackage][user] = collections.OrderedDict()
                    p[dept][standing][swPackage][user]['launches'] = userLaunches
                    p[dept][standing][swPackage][user]['launchFraction'] = userLaunches/float(p[dept][standing][swPackage]['launches'])

    return p

def getPackagesByUserByStandingByDepartmentData (source):
    if re.match("^file:",source,re.I):
        source = re.sub("^file:",'',source)
        with open(source) as data_file:
            jsonDataFromFile = json.load(data_file)['aggregations']
        return jsonDataFromFile
    elif re.match("^http:",source,re.I):
        import urllib
        import urllib2
        esQueryString = esQueryPackagesByUserByStandingByDepartment()
        jstring = urllib2.urlopen(source,esQueryString).read()
        jsonDataFromURL = json.loads(jstring)['aggregations']
        #jsonDataFromURL = ""
        return jsonDataFromURL
    else:
        return None

def esQueryPackagesByUserByStandingByDepartment ():
    q=r'''{
"query": {
"filtered": {
 "filter": {
   "bool": {
     "must_not": [
       {
         "terms": {
           "username": [
             "root"
           ]
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username: *1*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username:*2*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username: *0*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username:*3*"
             }
           },
           "_cache": true
         }
       },
       {
         "terms": {
           "exe.raw": [
'''
    for exclusion in exclusions.winexclude():
        exclusion = re.sub(r'\\', r'\\\\',exclusion)
        q = q + '"'+exclusion+'",\n'
    q = re.sub(',$','',q)
    q = q + '''
           ]
         }
       }
     ]
   }
 }
 }
 },
"aggs": {
	"department": {
		      "terms": {
		      	       "field" : "acadPlanFieldDescr1.raw",
			       "size": 25
			       },
			"aggs": {
				"classStanding": {
						  "terms": {
						  	 "field": "acadLevelDescription.raw"
							   },
				  "aggs": {
				  	  "swExe": {
					 	  "terms" : {
						    	  "field" :  "exe.raw",
                                                          "size"  : 5
						  	  },
								"aggs": {
									"users": {
										"terms" : {
											"field" : "username",
											"size"  : 5
										},
							"aggs": {
								"user_count": {
									"cardinality": {
										"field": "username"
										}
									}
	 			  	  			}
							}
						}
					}
	 		          	}
				}
			  }
		}
	}
}
    '''
    return q

if __name__ == "__main__":

    dataSource = "file:pkg_by_standing.out"
    dataSource = "http://localhost:9200/logstash-caen-labs-v1-*/_search"

    jsonData = getPackagesByUserByStandingByDepartmentData(dataSource)

    packages = pkgByUserByStandingByDepartmentParse(jsonData)

    print '''#+TITLE: CAEN Lab Software Launches: Fall 2014
#+OPTIONS: ':t ^:{} author:nil date:nil toc:t
#+LATEX_CLASS_OPTIONS: [12pt]
#+LATEX_HEADER: \usepackage{newcent}
    '''

    for dept in sorted(packages):
        #print dept
        print "\n\n*",dept
        for standing in packages[dept]:
            if (standing != 'launches' and standing != 'launchFraction'):
                # print "\t{0:15}  ({1:4.1f}%)".format(standing, float(packages[dept][standing]['launchFraction'])*100)
                print "\n** {0:15}  ({1:.1f}%)".format(standing, float(packages[dept][standing]['launchFraction'])*100)
                for package in packages[dept][standing]:
                    if (package != 'launches' and package != 'launchFraction'):
                        # print "\t\t{0:90}  ({1:4.1f}%)".format(package, float(packages[dept][standing][package]['launchFraction'])*100)
                        print " - ={0}=  ({1:.1f}%)".format(package, float(packages[dept][standing][package]['launchFraction'])*100)
                        for user in packages[dept][standing][package]:
                            if (user != 'launches' and user != 'launchFraction'):
                                # print "\t\t\t{0:8s}  ({1:4.1f}%)".format(user, float(packages[dept][standing][package][user]['launchFraction'])*100)
                                print "    - {0:8s}  ({1:.1f}%)".format(user, float(packages[dept][standing][package][user]['launchFraction'])*100)
