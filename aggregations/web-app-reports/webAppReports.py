#   -*- mode: python; indent: 4 -*-
from flask import Flask
from flask import render_template
from flask import request   # to read ?key=value from the URL
from flask import Response, stream_with_context  # to craft json and csv responses
import sys
import json
import datetime
# pip install plotly
import plotly.plotly as py
from plotly.graph_objs import *
from exclusions import winexclude, linuxexclude
from elasticSearch import buildJsonQuery, esSearch


"""
This script is intended to render the results of an Elasticsearch
aggregation as a table in a web page.

To use it in the CAEN environment of 14 November 2014, you need to do
a couple of things.

  #. pip install flask
  #. set up tunnels for ES:9200
  #. cd operdata/aggregations
  #. python web-app-reports.py
  #. open browser to http://localhost:4999
  #. view the amazing aggregations, not in json!!

If it doesn't work, try looking at:
      https://code.google.com/p/modwsgi/wiki/DebuggingTechniques
"""

###### Configuration, read by app.config.from_object ######
URL                   = 'http://flux-admin04.engin.umich.edu:9200/'
URLPATH               = 'logstash-caen-labs-v1-*/_search'
LINUX_SEARCH_STRING   = '"linux"'
LINUX_TITLE           = 'Linux'
WINDOWS_SEARCH_STRING = '"windows", "winevent"'
WINDOWS_TITLE         = 'Windows'
SEARCH_FIELD          = '"exe.raw"'
LISTEN_IP             = '0.0.0.0'      # 0.0.0.0 for running in WSGI
PKGPATH               = '/var/www/softwarereport/'
PKGSUFFIX             = '-packages.txt'
CREDS_FILE            = '/var/lib/plotly-creds.sec'
###########################################################


app = Flask(__name__)
app.config.from_object(__name__)

@app.route('/')
def index():
    """
    Make a simple index.html page
    """

    return render_template('index-template.html')

@app.route('/hello')
def hello():
    """
    Make a demo 'Hello World' page.
    """

    hw = "Hello World<br>\n"

    return hw


@app.route('/sessions/<os>')
def sessions(os, console=None,header=None,search=None):

    """
    Build up the JSON query (Tom has a better way that will
    hopefully replace this), get the results, convert it to some
    lists (because the Better Waywas beyond me at the moment),
    and send it all to the render_template to make some HTML
    """
    url     = app.config['URL']
    urlpath = app.config['URLPATH']

    exclude = False

    if (not console):
        output_format = request.args.get('format', '')
        exclude=(request.args.get('exclude'))

    if os == 'linux':
        title = app.config['LINUX_TITLE']
    else:
        title = app.config['WINDOWS_TITLE']

    # Sets display variables
    today = str(datetime.date.today())
    description="This is the list of " +title+" software based on the number of unique sessions as of " + today+"."
    title=title + " Software Sessions"
    header=['Executable', "Number of Sessions", "Number of Launches", "Average Number of Launches Per Session"]

    querytype="sessions"
    myarg  = buildJsonQuery(os,search, querytype, app)
    swlist = esSearch(url,urlpath,myarg, querytype)

    swtitle = []
    numsessions = []
    numstarts = []
    sessionave =[]
    csv = []
    if (header != False):
        csv.append("Title,  NumSessions,  NumLaunches, NumLaunchesPerSession\n")

    if exclude:
        winex= winexclude()
        linuxex= linuxexclude()
    else:
        winex=[]
        linuxex=[]

    for m in (sorted(swlist,key=lambda x:swlist[x]['session'],reverse=True)):
        if not (m in winex or m in linuxex):
            swtitle.append(m)
            session=int(swlist[m]['session'])
            starts=int(swlist[m]['starts'])
            numsessions.append(swlist[m]['session'])
            numstarts.append(swlist[m]['starts'])
            if session>0:
                sesave=(starts)/(session)
            else:
                sesave=starts
            sessionave.append(str(sesave))
            csv.append(m+","+swlist[m]['session']+","+swlist[m]['starts']+ ","+str(sesave)+"\n")

    if (console) :
        return (csv)

    if output_format == "json":
        swjson = json.dumps(swlist, indent=4, sort_keys=True)
        resp = Response(response=swjson, mimetype='application/json')
        return (resp)
    elif output_format == "csv":
        resp = Response(response=csv,mimetype='text/csv')
        return (resp)
    else:
        return render_template('table-template.html',
                               title=title,
                               description=description,
                               header=header,
                               today=today,
                               results=[swtitle, numsessions, numstarts, sessionave])


@app.route('/users/<os>')
def users(os, console=None,header=None,search=None):
    """
    Build up the JSON query (Tom has a better way that will
    hopefully replace this), get the results, convert it to some
    lists (because the Better Way was beyond me at the moment),
    and send it all to the render_template to make some HTML
    """
    url     = app.config['URL']
    urlpath = app.config['URLPATH']
    exclude=(request.args.get('exclude')) or False

    if (not console):
        output_format = request.args.get('format', '')

    if os == 'linux':
        title = app.config['LINUX_TITLE']
    else:
        title = app.config['WINDOWS_TITLE']
    today = str(datetime.date.today())
    description="This is the list of " +title+" software that is sorted by the number of unique users as of " + today+"."
    title=title + " Software Users"
    header=['Executable', "Number of Users", "Number of Launches", "Average Number of Launches Per User"]

    querytype = "users"
    myarg  = buildJsonQuery(os,search, querytype, app)
    swlist = esSearch(url,urlpath,myarg, querytype)
    today = str(datetime.date.today())
    swtitle = []
    numusers = []
    numstarts = []
    startsuser=[]
    csv = []
    if (header != False):
        csv.append("Title,  NumUsers,  NumLaunches, NumLaunchesPerUser\n")
    if exclude:
        winex= winexclude()
        linuxex= linuxexclude()
    else:
        winex=[]
        linuxex=[]

    for m in (sorted(swlist,key=lambda x:swlist[x]['users'],reverse=True)):
        if not (m in winex or m in linuxex):
            swtitle.append(m)
            users=int(swlist[m]['users'])
            starts=int(swlist[m]['starts'])
            numusers.append(swlist[m]['users'])
            numstarts.append(swlist[m]['starts'])
            if users>0:
                userave=(starts)/(users)
            else:
                userave=starts
            startsuser.append(str(userave))
            csv.append(m+","+swlist[m]['users']+","+swlist[m]['starts']+","+str(userave) +"\n")

    if (console) :
        return(csv)

    if output_format == "json":
        swjson = json.dumps(swlist, indent=4, sort_keys=True)
        resp = Response(response=swjson, mimetype='application/json')
        return (resp)
    elif output_format == "csv":
        resp = Response(response=csv, mimetype='text/csv')
        return (resp)
    else:
        return render_template('table-template.html',
                                title=title,
                               description=description,
                               header=header,
                               today=today,
                               results=[swtitle, numusers, numstarts, startsuser])

@app.route('/packages/<os>')
def packages(os):

    if (os == 'windows'):
        retString = "<p>I'm sorry, we don't have package data for Windows yet.  Check"
        retString = retString + " with caen-data-analytics@umich.edu for more detail.</p>"
        return (retString)

    csv = users(os,"csv",False,"package.raw")

    filename = app.config['PKGPATH']+os+app.config['PKGSUFFIX']
    file = open(filename, "r")
    packages = file.readlines()

    pkgStats = getPackageStats(csv,packages)

    pkgBins = binData(pkgStats)

    iframeString = makeIframeString(pkgBins,pkgStats)

    return render_template('web-pkg-template.html',
                           os=os.title(),
                           iframeString=iframeString)

@app.route('/deptstanding')
def deptstanding():
    import pkg
    url     = app.config['URL']+app.config['URLPATH']

    jsonData = pkg.getPackagesByUserByStandingByDepartmentData(url)
    packages = pkg.pkgByUserByStandingByDepartmentParse(jsonData)

    # deptStandingData = "<pre>" + json.dumps(packages, sort_keys=True, indent=4) + "</pre>"

    title = ' Software Launches by Department, Package, and User'

    description = ''' The following is a list of executables ordered by the number of
    launches per academic standing level per major.  '''

    return render_template('pkgByDeptStanding.html',
                           title = title,
                           description = description,
                           packages = packages)

def console_output(os):
    csv = sessions(os,"csv",True,None)
    for m in csv:
        sys.stdout.write(m)

def binData(pkg):
    bins={}
    mostUsers = 51
    incr = 1
    for number in range(0,mostUsers,incr):
        if (number not in bins):
            bins[number] = 0
        for p in pkg:
 #           if (number-incr) < pkg[p] <= number:   # to bin ranges
            if pkg[p] <= number:                  # to count sums
                bins[number] = bins[number] + 1
    return(bins)

def getPackageStats (csv,pkgs):
    ranList = {}
    for m in csv:
        m = m.strip()
        title = m.split(",")[0]
        users = int(m.split(",")[1])
        starts = int(m.split(",")[2])
        ranList[title] = users

    pkg={}
    for m in pkgs:
        m = m.strip()
        pkg[m]=0
        if m in ranList:
            pkg[m]=ranList[m]

    return(pkg)

def makeIframeString(bins,pkg):

    creds=[]
    with open(app.config['CREDS_FILE']) as f:
        creds = [x.strip('\n') for x in f.readlines()]

    res = py.sign_in(creds[0], creds[1])

    trace1 = Scatter(
        x= bins.keys(),
        y= bins.values(),
        fill='tozeroy',
        fillcolor="red",
        opacity=0.52,
        name="Unused Packages"
        )
    trace2 = Scatter(
        x= bins.keys(),
        y= [len(pkg)] * len(bins),
        fill='tonexty',
        fillcolor="green",
        opacity=0.52,
        name="Total Installed Packages"
        )
    layout = Layout(
        title='Number of CAEN Linux Packages versus Unique Users',
        xaxis=XAxis(
            title='Unique Users Cut-off',
            titlefont=Font(
                size=18,
                color='#7f7f7f'
            )
        ),
        yaxis=YAxis(
            title='Number of Packages',
            titlefont=Font(
                size=18,
                color='#7f7f7f'
            )
        )
    )
    data = Data([trace1,trace2])
    fig = Figure(data=data, layout=layout)
    plot_url = py.plot(fig, filename='linux-sw-packages', auto_open=False)
    iframe_string = '<iframe id="igraph" style="border:none" src="'
    iframe_string = iframe_string+plot_url+'/550/550" width="100%" height="700"></iframe>'

    return(iframe_string)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--os", action="store", dest="opsys",
                        choices=['linux', 'windows'], const='windows',
                        help="chose an operating system on which to "+
                        "report software usage; implies console output "+
                        "and defaults to 'windows'",
                        nargs='?')
    args = parser.parse_args()
    app.config['PKGPATH'] = ''

    ###### Configuration, read by app.config.from_object ######
    URL           = 'http://localhost:9200/'
    LISTEN_IP     = ''                    # '' == localhost
    CREDS_FILE    = 'plotly-creds.sec'    # for local
    LISTEN_PORT   = 4999
    LISTEN_IP     = ''      # set to '' for only localhost
    PKGPATH       = './'
    ###########################################################
    app.config.from_object(__name__)

    if (args.opsys):
        console_output(args.opsys)
    else:
        app.run(host=app.config['LISTEN_IP'],port=app.config['LISTEN_PORT'], debug=True)
