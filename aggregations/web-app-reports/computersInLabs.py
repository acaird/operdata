
import re
import json
from pprint import pprint

'''
The interpretation of this that works with ldapsearch on a Mac is:

ldapsearch -h adsroot.itcs.umich.edu -p 389 -x -D "acaird@umich.edu" -W -b 'OU=Instructional Edition,OU=2014 (Win7-x64),OU=CAEN Lab Software Environment,OU="CAEN Managed Desktops",OU=CAEN,OU=ENGIN,OU=Organizations,OU=UMICH,DC=adsroot,DC=itcs,DC=umich,DC=edu' -z 5 name

The "-z 5" limits the response to 5 computers, but they aren't all computers, and there are
more than 1000 computers so the default of no "-z" stops at 1000.

This one is even better because it restricts the OU one more level and only returns things
of objectClass 'computer':

ldapsearch -LLL -h adsroot.itcs.umich.edu -p 389 -x -D "acaird@umich.edu" -W -b 'OU=CAEN,OU=Instructional Edition,OU=2014 (Win7-x64),OU=CAEN Lab Software Environment,OU="CAEN Managed Desktops",OU=CAEN,OU=ENGIN,OU=Organizations,OU=UMICH,DC=adsroot,DC=itcs,DC=umich,DC=edu' -z 2000 "(&(name=*)(objectClass=computer))" cn

Turns out restricting things to that extra OU is limiting, although I don't understand why
yet, so these are the two correct queries, for Instruction and Research, although now
Instruction returns more than 1000 results, which is too big for AD to share with us.

ldapsearch -LLL -h adsroot.itcs.umich.edu -p 389 -x -D "acaird@umich.edu" -W -b 'OU=Research Edition,OU=2014 (Win7-x64),OU=CAEN Lab Software Environment,OU="CAEN Managed Desktops",OU=CAEN,OU=ENGIN,OU=Organizations,OU=UMICH,DC=adsroot,DC=itcs,DC=umich,DC=edu' -z 2000 "(&(name=*)(objectClass=computer))" cn | grep cn: > someResHosts

ldapsearch -LLL -h adsroot.itcs.umich.edu -p 389 -x -D "acaird@umich.edu" -W -b 'OU=Instructional Edition,OU=2014 (Win7-x64),OU=CAEN Lab Software Environment,OU="CAEN Managed Desktops",OU=CAEN,OU=ENGIN,OU=Organizations,OU=UMICH,DC=adsroot,DC=itcs,DC=umich,DC=edu' -z 2000 "(&(name=*)(objectClass=computer))" cn | grep cn: > somehosts


From: Don Lambert
To: Caird, Andrew
Subject: AD and LDAP
Date: Fri Jan 16 14:27:12 2015

I think these are the bits of information you need to get going with
an AD and LDAP query.

server:  adsroot.itcs.umich.edu
port 389 or 636
base dn:
bind as yourself

OU=Instructional Edition,OU=2014 (Win7-x64),OU=CAEN Lab Software Environment,OU=CAEN Managed Desktops,OU=CAEN,OU=ENGIN,OU=Organizations,OU=UMICH,DC=adsroot,DC=itcs,DC=umich,DC=edu

OU=Research Edition,OU=2014 (Win7-x64),OU=CAEN Lab Software Environment,OU=CAEN Managed Desktops,OU=CAEN,OU=ENGIN,OU=Organizations,OU=UMICH,DC=adsroot,DC=itcs,DC=umich,DC=edu

'''
def getADcomputersViaLDAP(ldapU,ldapP):
    import simpleldap

    ldapServer='adsroot.itcs.umich.edu'
    ldapPort=389
    caenOU =r'OU=2014 (Win7-x64),OU=CAEN Lab Software Environment,OU="CAEN Managed Desktops",OU=CAEN,OU=ENGIN,OU=Organizations,OU=UMICH,DC=adsroot,DC=itcs,DC=umich,DC=edu'
    ldapFilter='(&(name=*)(objectClass=computer))'
    searchTerms=['cn']

    edition={"instructional":"OU=Instructional Edition","research":"OU=Research Edition"}

    for ed in sorted(edition):
        baseDN=edition[ed]+","+caenOU
        print baseDN

    #baseDN=edition['instructional']+","+caenOU
    with simpleldap.Connection(ldapServer) as conn:
        is_valid = conn.authenticate(ldapU, ldapP)
        sls = conn.search(ldapFilter, base_dn=baseDN,attrs=searchTerms)

    print "LDAP bind:",is_valid
    pprint(sls)



'''
This block gracelessly stolen from:
 http://stackoverflow.com/questions/5967500/how-to-correctly-sort-a-string-with-a-number-inside
'''
def atoi(text):
    return int(text) if text.isdigit() else text
def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]
'''
 ~~~~~ End of the stolen block ~~~~~
'''

def parseBldgRoomComputers(jsonData):
    computers = {}
    for building in jsonData['building']['buckets']:
        computers[building['key']] = {}
        for room in building['room']['buckets']:
            computers[building['key']][room['key']] = []
            for computer in room['computer']['buckets']:
                computers[building['key']][room['key']].append(computer['key'])
            computers[building['key']][room['key']] = sorted(computers[building['key']][room['key']],key=natural_keys)
    return(computers)

def getComputers (source):
    if re.match("^file:",source,re.I):
        source = re.sub("^file:",'',source)
        with open(source) as data_file:
            jsonDataFromFile = json.load(data_file)['aggregations']
        return jsonDataFromFile
    elif re.match("^http:",source,re.I):
        import urllib
        import urllib2
        esQueryString = esQueryBldgRoomComputers()
        jstring = urllib2.urlopen(source,esQueryString).read()
        jsonDataFromURL = json.loads(jstring)['aggregations']
        #jsonDataFromURL = ""
        return jsonDataFromURL
    else:
        return None


def esQueryBldgRoomComputers ():
    q=r'''{
"query": {
"filtered": {
 "filter": {
   "bool": {
    "must": [
      {
       "range": {
         "@timestamp": {
           "from": "now-1w",
           "to": "now"
	}
       }
      }
     ],
     "must_not": [
       {
         "terms": {
           "username": [
             "root"
           ]
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username: *0*",
               "query": "username: *1*",
               "query": "username: *2*",
               "query": "username: *3*"
             }
           },
           "_cache": true
         }
       }
     ]
   }
 }
 }
 },
  "aggs": {
     "building": {
         "terms": {
	   "field":"building"
         },
	 "aggs": {
	   "room": {
	     "terms":{
	       "field":"room"
             },
	     "aggs": {
	       "computer": {
	         "terms":{
		   "field":"shorthost.raw"
	         }
	       }
             }
           }
         }
     }
  },
  "size": 0
}'
    '''
    return q



if __name__ == "__main__":

    creds=[]
    with open('./ldapcreds.sec') as f:
        creds = [x.strip('\n') for x in f.readlines()]

    getADcomputersViaLDAP(creds[0],creds[1])

    exit(0)

    dataSource = "file:computersInLabs.json"
    dataSource = "http://localhost:9200/logstash-caen-labs-v1-*/_search"

    jsonData = getComputers(dataSource)

    computers = parseBldgRoomComputers(jsonData)

    for building in sorted(computers):
        print building
        for room in sorted(computers[building]):
            print "\t",room
            for computer in computers[building][room]:
                print "\t\t",computer
