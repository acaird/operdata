excludelist = {}
excludelist['windows']= [
r'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe',
r'C:\Program Files\Windows Mail\WinMail.exe',
r'\Device\Mup\adsroot.itcs.umich.edu\sysvol\adsroot.itcs.umich.edu\scripts\engin\nircmdc.exe',
r'C:\Program Files\Windows Sidebar\sidebar.exe',
r'C:\Program Files\OpenAFS\Client\Program\afscreds.exe',
r'C:\Program Files\NVIDIA Corporation\nView\nwiz.exe',
r'C:\Program Files\Microsoft Security Client\msseces.exe',
r'C:\Program Files\Microsoft Office\Office15\msoia.exe',
r'C:\Program Files\Heimdal\bin\krbcc64s.exe',
r'C:\Program Files (x86)\Windows Mail\WinMail.exe',
r'C:\Program Files (x86)\PaperCut MF Client\pc-client.exe',
r'C:\Program Files (x86)\National Instruments\NI-DAQ\HWConfig\nidevmon.exe',
r'C:\Program Files (x86)\Google\Chrome\Application\39.0.2171.71\Installer\chrmstp.exe',
r'C:\Program Files (x86)\Common Files\Adobe\CS6ServiceManager\CS6ServiceManager.exe',
r'C:\Program Files (x86)\Box\Box Edit\Box Edit.exe',
r'C:\Program Files (x86)\Adobe\Acrobat 11.0\Acrobat\acrotray.exe',
r'C:\Python27\ArcGIS10.2\python.exe',
r'C:\Program Files\VMware\VMware View\Agent\bin\VMWVvphelper.exe',
r'C:\Program Files\VMware\VMware Tools\vmtoolsd.exe',
r'C:\Program Files\TortoiseSVN\bin\TSVNCache.exe',
r'C:\Program Files\Realtek\Audio\HDA\RAVCpl64.exe',
r'C:\Program Files\Common Files\VMware\Teradici PCoIP Server\VMwareViewClipboard.exe',
r'C:\Program Files\VMware\VMware View\Agent\bin\wssm.exe',
r'C:\Program Files\VMware\VMware Tools\TPAutoConnect.exe',
r'C:\Program Files\NVIDIA Corporation\Display\nvtray.exe',
r'C:\Program Files (x86)\Google\Chrome\Application\39.0.2171.65\Installer\setup.exe',
r'C:\Program Files (x86)\Adobe\Reader 11.0\Reader\AcroRd32.exe',
r'C:\Program Files\SolidWorks Corp\SolidWorks\sldShellExtServer.exe',
r'C:\Program Files (x86)\Google\Chrome\Application\38.0.2125.111\Installer\\chrmstp.exe',
r'C:\Program Files (x86)\Google\Chrome\Application\37.0.2062.124\nacl64.exe',
r'C:\Program Files (x86)\Google\Chrome\Application\38.0.2125.111\nacl64.exe',
r'C:\Program Files (x86)\Adobe\Acrobat 11.0\Acrobat\acrotray.exe',
r'C:\Program Files\Realtek\Audio\HDA\RAVCpl64.exe'
]

excludelist['linux']= [
r'/usr/sbin/sshd',
]


def winexclude():
    return excludelist['windows']

def linuxexclude():
    return excludelist['linux']

def exclude(os):
    if os in excludelist:
        return excludelist[os]
    else:
        return None
