Running this on a Mac is a little complicated, because Docker is running
in a VirtualBox VM, so there are a lot of layers to get through to get
networking working.

* Building

Build your image with the Dockerfile that is in this directory (=.=) and
give it the name (or tag) =acaird/flask= by typing:

#+BEGIN_EXAMPLE
     docker build -t "acaird/flask" .
#+END_EXAMPLE

The tag (the thing in the quotes) can be anything; see the Docker
documentation for more on naming conventions, versioning, etc.

* Getting to the port on your Mac

Now you have to get the port (port 5000, in our example) forwarded from
your computer to the VirtualBox host's port. Open the VirtualBox GUI and
select the computer called =boot2docker-vm= from the list on the left.
Then choose *Settings* from the *Machine* menu (or press Command-S). In
the *Settings* window, choose the *Network* icon at the top, then click
the *Port Forwarding* button. In the table that is presented, click the
weird looking little =+= sign on the right to add a rule. You can name
the rule anything, but type in =127.0.0.1= for the =Host IP= column, and
=5000= in both the =Host Port= and =Guest Port= columns. You can leave
the =Guest IP= field empty.

* Running

To start an image and do the Docker-to-VirtualBox port-forwarding, type:

#+BEGIN_EXAMPLE
     docker run -t -i -p 5000:5000 acaird/flask
#+END_EXAMPLE

This will start an instance, forward port 5000 between the VirtualBox
host and the Docker process, and present you with a shell prompt in the
Docker process.

From here you can start the test Flask server by typing:

#+BEGIN_EXAMPLE
     . /opt/rh/python27/enable
     cd /var/www/softwarereports
     python2.7 web-app-report.py
#+END_EXAMPLE

This loads the Python v2.7 environment and starts the Flask app with
Python's web server. If all went well, you should see:

#+BEGIN_EXAMPLE
     * Running on http://0.0.0.0:5000/
#+END_EXAMPLE

And if everything went /super/ well, you should be able to open a web
browser on your computer, go to =http://localhost:5000= and see your
Flask app's =@app.route('/')= index page.

* OpsWorks Installation

  - use the forked opsworks git repo for OpsWorks (yes, I know this is a
    crappy instruction, I'll spruce it up later ~--acaird~)
  - do these things that should be done by Chef:
    - install flask and plotly on the EC2 instance:
      =easy install flask plotly=
    - copy some files, like:
      #+BEGIN_EXAMPLE
       scp -i YourPEMfile.pem ec2-user@ec2.your.host webserver/flask-virthost.conf webserver/softwarereport.wsgi webAppReports.py ec2-user@ec2-54-204-232-242.compute-1.amazonaws.com:
       scp -r -i YourPEMfile.pem ec2-user@ec2.your.host exclusions.py elasticSearch.py linux-packages.txt templates ec2-user@ec2-54-204-232-242.compute-1.amazonaws.com:
      #+END_EXAMPLE
    - on the EC2 host, put those files in place:
      #+BEGIN_EXAMPLE
       cd ~
       sudo cp -r * /var/www/softwarereport/
       sudo rm /var/www/softwarereport/flask-virthost.conf
       sudo cp flask-virthost.conf /etc/httpd/conf.d/
      #+END_EXAMPLE
      then restart the web server:
      #+BEGIN_EXAMPLE
       sudo /etc/init.d/httpd restart
      #+END_EXAMPLE
