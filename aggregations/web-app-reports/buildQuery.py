from re import sub
from exclusions import exclude

def buildExclusions(os,searchterm):
    a = '{\n'                    #\
    a = a + '  "terms": {\n'     # > start the braces and query language
    a = a + '    "'+searchterm+'": [\n' #/

    for exe in exclude(os):      # add all of the exes
        a = a + '     "'+exe+'",\n'

    a = sub(',$','',a)        # strip off last comma
    a = sub(r"\\",r"\\\\",a)  # substitute \\ for \ for Windows paths

    a = a + '    ]\n   }\n}'     # close all the braces

    return a

if __name__ == "__main__":
    # In case we want to test this or produce exclusion
    # lists from the command line, we can with this
    import argparse
    parser = argparse.ArgumentParser(formatter_class=
                                     argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--os", "-o", default="windows",
                        choices=('windows', 'linux'),
                        help="select the OS for which to build "+
                        "the exclusion list (windows or linux)")
    parser.add_argument("--search", "-s", default="exe.raw",
                        help="select the Elasticsearch term for which to"+
                        "search")
    args = parser.parse_args()

    excludeJson = buildExclusions(args.os, args.search)

    print excludeJson
