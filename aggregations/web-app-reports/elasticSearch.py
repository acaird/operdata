#   -*- mode: python; indent: 4 -*-
from flask import Flask
from flask import render_template
from flask import request   # to read ?key=value from the URL
from flask import Response, stream_with_context  # to craft json and csv responses
import sys
import json
import datetime
# pip install plotly
import plotly.plotly as py
from plotly.graph_objs import *
from exclusions import winexclude, linuxexclude


def getJsonFromElasticSearch (url,arg):
    """
    Contact the Elasticsearch server, load the json into a dictionary
    """
    import urllib
    import urllib2
    jstring = urllib2.urlopen(url, arg).read()
    return(json.loads(jstring)['aggregations']['comm_users']['buckets'])

def esSearch (url, urlpath, query, querytype):
    """
    Build the URL path and send it and the query to get the data;
    this is separate so that the data can be read from other
    places (like a file) if needed.

    Once the data is here, make a smaller dictionary with more
    useful keys and some embedded formatting for text output
    """
    fullurl = url+urlpath
    jdata = getJsonFromElasticSearch(fullurl, query)
    sw = {}
    swlist = []
    string = []
    for m in jdata:
        # if m['key'] == 'mpirun':
        #         continue
        # if m['key'] == 'orted':
        #         continue
        title = '{:>20s}'.format(m['key'].encode('ascii',errors='ignore'))
        if not sw.has_key(title):
            sw[title] = {}
        if querytype=="users":
            sw[title]['users']  = '{:6d}'.format(m['user_count']['value'])
        if querytype=="sessions":
            sw[title]['session']='{:6d}'.format(m['session_count']['value'])
        sw[title]['starts'] = '{:8d}'.format(m['doc_count'])
        sw[title]['group'] = ""


    return(sw)

def buildJsonQuery(os,search,querytype, app):
    myarg='''{
      "query" : {
        "filtered" : {
          "filter": {
              "bool": {
                  "must": [
                      {
                          "terms": {
                              "_type": [
        '''
    if os == 'linux':
        myarg = myarg + app.config['LINUX_SEARCH_STRING']
    else:
        myarg = myarg + app.config['WINDOWS_SEARCH_STRING']

    myarg = myarg + '''
                              ]
                          }
                      }
                  ]
              }
          }
        }
      },
     "aggregations": {
       "comm_users": {
         "terms": { '''

    if (not search):
        myarg = myarg + '"field":' + app.config['SEARCH_FIELD']  + ','
    else:
        myarg = myarg + '"field":'+'"'+search+'",'

    myarg = myarg + '''
          "size": 0
         },
       "aggregations": { '''
    if querytype=="users":
        myarg = myarg + '''
       "user_count": {
         "cardinality": {
           "field": "username"      }     }     }     }     }
     }'''
    if querytype=="sessions":
        myarg = myarg + '''
       "session_count": {
         "cardinality": {
           "field": "sessionid"      }     }     }     }     }
     }'''

    return(myarg)

def deptStandingPkgUserJSONquery():
    '''
    The whole block of things like:
       {
         "terms": {
           "exe.raw": [
             "C:\\Python27\\ArcGIS10.2\\python.exe"
           ]
         }
       },
    should be constructed from the exclusions.py file; in fact, that should happen
    in every place, and let ES do the work of the exclusions, so it sends less data
    to us to process.

    Also, the list below should be vetted with exclusions.py because I like some of
    the things I've excluded.  -acaird
    '''

    fixedQuery = '''{
"query": {
"filtered": {
 "filter": {
   "bool": {
     "must_not": [
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe"
           ]
         }
       },
       {
         "terms": {
           "username": [
             "root"
           ]
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username: *1*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username:*2*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username: *0*"
             }
           },
           "_cache": true
         }
       },
       {
         "fquery": {
           "query": {
             "query_string": {
               "query": "username:*3*"
             }
           },
           "_cache": true
         }
       },
       {
         "terms": {
           "exe.raw": [
	     "C:\\Program Files (x86)\\Google\\Chrome\\Application\\39.0.2171.65\\Installer\\setup.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\Microsoft Office\\Office15\\msoia.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files (x86)\\Adobe\\Reader 11.0\\Reader\\AcroRd32.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\SolidWorks Corp\\SolidWorks\\sldShellExtServer.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Python27\\ArcGIS10.2\\python.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files (x86)\\Common Files\\Adobe\\CS6ServiceManager\\CS6ServiceManager.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\Heimdal\\bin\\krbcc64s.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
	     "C:\\Program Files (x86)\\Google\\Chrome\\Application\\38.0.2125.111\\Installer\\chrmstp.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
	     "C:\\Program Files (x86)\\Google\\Chrome\\Application\\37.0.2062.124\\nacl64.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
	     "C:\\Program Files (x86)\\Google\\Chrome\\Application\\38.0.2125.111\\nacl64.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files (x86)\\Adobe\\Acrobat 11.0\\Acrobat\\acrotray.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\Microsoft Security Client\\msseces.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\OpenAFS\\Client\\Program\\afscreds.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files (x86)\\Box\\Box Edit\\Box Edit.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files (x86)\\PaperCut MF Client\\pc-client.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files (x86)\\PaperCut MF Client\\pc-client.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
	     "C:\\Program Files\\Microsoft Office\\Office15\\msoia.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "\\Device\\Mup\\adsroot.itcs.umich.edu\\sysvol\\adsroot.itcs.umich.edu\\scripts\\engin\\nircmdc.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
	     "C:\\Program Files (x86)\\Adobe\\Acrobat 11.0\\Acrobat\\acrodist.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
	     "C:\\Program Files\\NVIDIA Corporation\\nView\\nwiz.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\NVIDIA Corporation\\Display\\nvtray.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
	     "C:\\Program Files (x86)\\National Instruments\\NI-DAQ\\HWConfig\\nidevmon.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\TortoiseSVN\\bin\\TSVNCache.exe"
           ]
         }
       },
       {
         "terms": {
           "exe.raw": [
             "C:\\Program Files\\Realtek\\Audio\\HDA\\RAVCpl64.exe"
           ]
         }
       }
     ]
   }
 }
 }
 },
"aggs": {
	"department": {
		      "terms": {
		      	       "field" : "acadPlanFieldDescr1.raw",
			       "size": 25
			       },
			"aggs": {
				"classStanding": {
						  "terms": {
						  	 "field": "acadLevelDescription.raw"
							   },
				  "aggs": {
				  	  "swExe": {
					 	  "terms" : {
						    	  "field" :  "exe.raw",
                                                          "size"  : 5
						  	  },
								"aggs": {
									"users": {
										"terms" : {
											"field" : "username",
											"size"  : 5
										},
							"aggs": {
								"user_count": {
									"cardinality": {
										"field": "username"
										}
									}
	 			  	  			}
							}
						}
					}
	 		          	}
				}
			  }
		}
	}
}
    '''
    return (fixedQuery)
