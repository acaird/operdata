# aggregation to order package by unique user
# ignores chrome.exe and net.exe

curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty' -d '{
	"query": {
		"bool": {
			"must_not": [
				{"match": { "comm": "chrome.exe" }},
				{"match": { "comm": "net.exe"}}
			]
		}
	},
	"aggs": {
		"comm_users": {
			"terms": {
				"field": "comm",
				"size": 10,
				"order": {
					"user_count" : "desc"
				}
			},
			"aggs": {
				"user_count": {
					"cardinality": {
						"field": "username"
					}
				}
			}
		}
	}
}'
