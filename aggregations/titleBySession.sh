curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty' -d ' {
	"aggs": {
		"filtering": {
			"filter": {
				"bool": {
					"must_not": {
						"term": {
							"comm": [
								"AcroRd32.exe",
								"Box Edit.exe",
								"CS6ServiceManager.exe",
								"RAVCpl64.exe",
								"TPAutoConnect.exe",
								"TSVNCache.exe",
								"VMWVvphelper.exe",
								"VMwareViewClipboard.exe",
								"WinMail.exe",
								"acrotray.exe",
								"afscreds.exe",
								"chrmstp.exe",
								"chrome.exe",
								"msoia.exe",
								"msseces.exe",
								"nacl64.exe",
								"nidevmon.exe",
								"nircmdc.exe",
								"nvtray.exe",
								"nwiz.exe",
								"pc-client.exe",
								"python.exe",
								"setup.exe",
								"sidebar.exe",
								"sldShellExtServer.exe",
								"vmtoolsd.exe",
								"wssm.exe",
								"AdobeARM.exe",
								"acrodist.exe",
								"USBDLM_usr.exe",
								"reader_sl.exe",
								"acrobat_sl.exe",
								"GoogleUpdate.exe",
								"USBDLM.exe",
								"VCRT_check.exe",
								"matlab.exe",
								"intelremotemonagent.exe",
								"agent.exe"
							]
						}
					}
				}
			}
		},
		"titles": {
			"terms": {
				"field": "comm.raw",
				"size": 20,
				"order": {
					"user_count" : "desc"
				}
			},
			"aggs": {
				"user_count": {
					"cardinality": {
						"field": "sessionid"
					}
				}
			}
		}
	}
}'