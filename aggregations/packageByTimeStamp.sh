# orders packages by number of unique users
# prints last five time stamps per package with username

curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty' -d '{
	"aggs": {
		"times": {
			"terms": {
				"field": "comm",
				"size": 10,
				"order": { "top_users" : "desc" }
			},
			"aggs": {
				"top_users": {
					"cardinality": {
						"field": "username"
					}
				},
				"timestamp": {
					"terms": {
						"field": "@timestamp",
						"size": 5
					},
					"aggs": {
						"users": {
							"terms": {
								"field": "username"
							}
						}
					}
				}
			}
		}
	}
}'