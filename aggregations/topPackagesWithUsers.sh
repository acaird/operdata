curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty' -d '{
	"aggs": {
		"comm_users": {
			"terms": {
				"field": "comm",
				"size": 10,
				"order": { "top_users" : "desc" }
			},
			"aggs": {
				"top_users": {
					"cardinality": {
						"field": "username"
					}
				},
				"users": {
					"terms": {
						"field": "username",
						"size": 5
					}
				}
			}
		}
	}
}'