curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty' -d ' {
	"aggs": {
		"comms": {
			"terms": {
				"field": "comm.raw",
				"size": 10,
				"order": {
					"user_count" : "asc"
				}
			},
			"aggs": {
				"user_count": {
					"cardinality": {
						"field": "username"
					}
				}
			}
		}
	}
}'