# aggregation to order package by number of logonids
# then prints user with largest number of logonids associated with uniqname

curl -XGET 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty' -d '{
	"aggs": {
		"packages_by_logonids": {
			"terms": {
				"field": "comm",
				"size": 10,
				"order": {
					"logonid_amount" : "desc"
				}
			},
			"aggs": {
				"logonid_amount": {
					"cardinality": {
						"field": "logonid"
					}
				},
				"username": {
					"terms": {
						"field": "username",
						"size": 1,
						"order": {
							"user_logonid_amount": "desc"
						}
					},
					"aggs": {
						"user_logonid_amount": {
							"cardinality": {
								"field": "logonid"
							}
						}
					}
				}
			}
		}
	}
}'
