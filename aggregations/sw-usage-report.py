import json
from optparse import OptionParser

def getJsonFromElasticSearch (url,arg):
        import urllib
        import urllib2
        jstring = urllib2.urlopen(url, arg).read()
        return(json.loads(jstring)['aggregations']['comm_users']['buckets'])

def getJsonFromFile (fileName):
        f = open(fileName,"r")
        jstring = f.read()
        return(json.loads(jstring)['aggregations']['comm_users']['buckets'])

def getMapData (mapFile):
        f = open(mapFile,"r")
        return(json.loads(f.read()))

url     = 'http://localhost:9200/'
#urlpath = 'flux-software-*/_search'
urlpath = 'logstash-caen-labs-v1-*/_search'


myarg='''
{
    "query" : {
      "filtered" : {
        "filter": {
            "bool": {
                "must": [
                    {
                        "terms": {
                            "_type": [
                                "windows", "winevent"
                            ]
                        }
                    }
                ]
            }
        }
      }
    },
    "aggregations": {
            "comm_users": {
                    "terms": {
                            "field": "exe.raw",
                            "size": 0
                    },
                    "aggregations": {
                            "user_count": {
                                    "cardinality": {
                                            "field": "username"
                                    }
                            }
                    }
            }
    }
}'''

parser = OptionParser()
parser.add_option("-f", "--file", action="store", type="string", dest="inputFile",
                  help="read json output from FILE instead of ElasticSearch", metavar="FILE")
parser.add_option("-p", "--plot", action="store", type="string", dest="plotFile",
                  help="write a plot to PLOTFILE", metavar="PLOTFILE")
parser.add_option("-u", "--url", action="store", type="string", dest="url",
                  help="contact URL for ElasticSearch data", metavar="URL")
parser.add_option("-m", "--mapfile", action="store", type="string", dest="mapFile",
                  help="use the JSON data in MAPFILE to map executable names to packages", metavar="MAPFILE")

(options, args) = parser.parse_args()

if (options.inputFile):
        jdata = getJsonFromFile(options.inputFile)
else:
        fullurl = url+urlpath
        jdata = getJsonFromElasticSearch(fullurl, myarg)

if (options.mapFile):
        mapdata = getMapData(options.mapFile)

sw = {}
swlist = []
for m in jdata:
        # if m['key'] == 'mpirun':
        #         continue
        # if m['key'] == 'orted':
        #         continue
        title = '{:>20s}'.format(m['key'].encode('utf-8',errors='ignore'))
        if not sw.has_key(title):
                sw[title] = {}
        sw[title]['users']  = '{:6d}'.format(m['user_count']['value'])
        sw[title]['starts'] = '{:8d}'.format(m['doc_count'])
        sw[title]['group'] = ""
        if (options.mapFile):
                for group in mapdata.iteritems():
                        for p in group[1]:
                                if (p == m['key']):
                                        sw[title]['group'] = group[0]
        # print title,sw[title]['users'],sw[title]['starts'],sw[title]['group']
        swlist.append(sw[title]['users'])


for m in (sorted(sw,key=lambda x:sw[x]['users'],reverse=True)):
        print m, sw[m]['users'], sw[m]['starts']

if (options.plotFile) :
	# When we switch to ggplot, this is the command from R that would be nice
	# ggplot(a, aes(x=a$V2, reorder(V1,V2))) + geom_point(aes(size=a$V3)) + theme(axis.text.y = element_text(size=2,colour="black"))
        import matplotlib.pyplot as plt
        x = range(len(swlist))
        y = []
        for m in x:
                y.append(swlist[m])
        plot = plt.plot (x,y,"ro",ms=2)
        plt.savefig (options.plotFile)
