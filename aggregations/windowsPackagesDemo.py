#!/usr/bin/env python
import json
import re
import pprint
from optparse import OptionParser

def getJsonFromElasticSearch (url,arg):
        import urllib
        import urllib2
        jstring = urllib2.urlopen(url, arg).read()
        return(json.loads(jstring)['facets']['terms']['terms'])

url     = 'http://localhost:9200/'
urlpath = 'logstash-caen-labs-v1-*/_search'

myarg='''
 {
  "facets": {
    "terms": {
      "terms": {
        "field": "exe.raw",
        "size": 1000000,
        "order": "count",
        "exclude": [
          "c",
          "program",
          "files",
          "x86"
        ]
      },
      "facet_filter": {
        "fquery": {
          "query": {
            "filtered": {
              "query": {
                "bool": {
                  "should": [
                    {
                      "query_string": {
                        "query": "_type:winevent"
                      }
                    }
                  ]
                }
              },
              "filter": {
                "bool": {
                  "must": [

                    {
                      "fquery": {
                        "query": {
                          "query_string": {
                            "query": "EventType=4689"
                          }
                        },
                        "_cache": true
                      }
                    }
                  ],
                  "must_not": [
                    {
                      "fquery": {
                        "query": {
                          "query_string": {
                            "query": "exe=chrome.exe"
                          }
                        },
                        "_cache": true
                      }
                    }
                  ]
                }
              }
            }
          }
        }
      }
    }
  },
  "size": 0
}
'''

parser = OptionParser()
parser.add_option("-u", "--url", action="store", type="string", dest="url",
                  help="contact URL for ElasticSearch data", metavar="URL")

(options, args) = parser.parse_args()

fullurl = url+urlpath
jdata = getJsonFromElasticSearch(fullurl, myarg)

pp = pprint.PrettyPrinter(depth=10)

countedPaths={}
for m in jdata:
        fullpath = m['term']
        fullpath = fullpath.rsplit('\\',1)[0]
        fullpath = re.sub('^C:\\\\',"",fullpath)
        fullpath = re.sub('Program Files\\\\',"",fullpath)
        fullpath = re.sub('Program Files \\(x86\\)\\\\',"",fullpath)
        if countedPaths.has_key(fullpath):
                countedPaths[fullpath] = countedPaths[fullpath] +1
        else:
                countedPaths[fullpath] = 1

for m in sorted(countedPaths, key=countedPaths.get):
        if countedPaths[m] > 10:
                print countedPaths[m],"       ",m
