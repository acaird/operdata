import json
import csv
import pprint
import sys

def getJsonFromElasticSearch(url, arg):
	import urllib
	import urllib2
	jstring = urllib2.urlopen(url, arg).read()
	return(json.loads(jstring)['aggregations']['exe']['buckets'])

url  = 'http://localhost:9200/logstash-caen-labs-v1-*/_search?pretty'

myarg='''
{
	"aggs": {
		"exe": {
			"terms": {
				"field": "exe.raw",
				"order": {
					"user_count": "desc"
				}
			},
			"aggs": {
				"user_count": {
					"cardinality": {
						"field": "username"
					}
				},
				"week_range": {
					"date_histogram": {
						"field": "@timestamp",
						"interval": "week",
						"min_doc_count": 0
					},
					"aggs": {
						"users_per_week": {
							"cardinality": {
								"field": "username"
							}
						}
					}
				}
			}
		}
	}
}
'''


# jdata = open('exe.json')
# data = json.load(jdata)['aggregations']['exe']['buckets']


inpt = raw_input("How many exe would you like to see?  Please enter a number 1 through 42. ")
numAggs = int(inpt)
data = getJsonFromElasticSearch(url, myarg);

exeList = []
dateList = []
for x in range(0, numAggs):
	name = data[x]['key']
	name += " " + str(data[x]['user_count']['value'])
	# name = '{:>20s}'.format(name)
	exeList.append(name.split(","))
	userCountList = []
	for y in range(0, 10):
		users = ""
		if(y > 0):
			users += " "
		users = str(data[x]['week_range']['buckets'][y]['users_per_week']['value'])
		userCountList.append(users)
	exeList.append(userCountList)

# dateList.append("Exe Name".split(","))
for x in range(0, 10):
	date = ""
	if(x > 0):
		date += " "
	date += data[0]['week_range']['buckets'][x]['key_as_string']
	prettydate = date[:date.rfind('T')]
	dateList.append(prettydate)
printList = []
printList.append("Exe Name".split(","))
printList.append(dateList)

with open("test2.csv", "wb") as csv_file:
	writer = csv.writer(csv_file)
	for line in printList:
		writer.writerow(line)
	for line in exeList:
		writer.writerow(line)

# jdata.close()
