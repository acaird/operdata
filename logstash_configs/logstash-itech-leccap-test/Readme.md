# Logstash for Lecture Capture Servers

##Apache Config

For our apache access logs, we're using a custom log format, based off combinedvhost:

```conf
LogFormat "%v %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" itechcombinedvhost

CustomLog "/var/log/httpd/ssl_access_log" itechcombinedvhost
```

I'm seeing some Grok Parse failures on the Apache Error logs, so that configuration may need some tweaks still.

##S3

I've pointed S3 logging at another bucket. Otherwise, operations on the log files within the bucket you are monitoring creates log entries.

While my example lists credentials inside of the S3 conf file, you are supposed to be able to set environment variables and have Logstash use those. I've filed a bug - it has to do with how the array type is validated, I beleive.
