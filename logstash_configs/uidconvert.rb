require "logstash/filters/base"
require "logstash/namespace"
require "rufus-scheduler"

class LogStash::Filters::UidConvert < LogStash::Filters::Base

# TODO: provide a config example
#

	config_name "uidconvert"

	# new plugins start at milestone 1
	milestone 1

	config :hashfile, :validate => :string, :required => true
	config :refresh, :validate => :string, :default => "3h" 

	public 
	def register
	# nothing to do right now
	# maybe a place to read in the hash...
		update_hash
#	pwin = File.new(hashfile, 'r')
#	@uidHash = Marshal.load(pwin.read())
#	pwin.close()
		scheduler = Rufus::Scheduler.new
		scheduler.every @refresh do
			update_hash
		end
	end # def register
	
	def update_hash
	        pwin = File.new(hashfile, 'r')
	        @uidHash = Marshal.load(pwin.read())
        	pwin.close()
		rfile = File.new(hashfile+'.lr', 'w')
		rfile.write(Time.now.inspect+"\n")
		rfile.close()
	end

	public
	def filter(event)
	# don't do anyhing unless it's an actual filter event
		return unless filter?(event)
		if not event['uid'].to_s.empty?
			event['username'] = @uidHash[event['uid'].to_s]
			filter_matched(event)
		end 
	end #def filter
end #clas LogStash::Filter::uidConvert
