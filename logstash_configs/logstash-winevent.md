
## Logstash for Windows Events

### Notes for testing with lab computers

#### Setting up logstash agent

**Logstash requirements**

- Logstash is a ruby project, but all ruby/jruby requirements are bundled
- Logstash requires Java 1.6 or greater
- `%JAVA_HOME%` must be set
- hosts that are ‘shipping’ logs to Redis host need ip 
permissions
	- host: 10.164.7.7 aka flux-admin03.engin.umich.edu
	- msbritt needs to know what ips or iprange to open port 6379
	- msbritt needs to confirm how we should ‘type’ and ‘tag’ data streams

**1. Enable additional windows logging**

- In an administrative shell open Local Group Policy Editor **gpedit.msc**
- Enable two auditing options indicated here ![](./logstash-winevent-local-group-policy.png)
	- Audit Process Creation on Success
	- Audit Process Termination on Success
- Test via **eventvwr** to make sure events are flowing

**2. Install logstash as agent**

- Get logstash and contribs
	- [https://download.elasticsearch.org/logstash/logstash/logstash-1.4.2.tar.gz](https://download.elasticsearch.org/logstash/logstash/logstash-1.4.2.tar.gz)
	- [https://download.elasticsearch.org/logstash/logstash/logstash-contrib-1.4.2.tar.gz](https://download.elasticsearch.org/logstash/logstash/logstash-contrib-1.4.2.tar.gz)
- Unzip into a directory of your choosing e.g. c:\temp\logstash-agent
- **Note:** unzip logstash first, then unzip contribs ‘over the top’ of the logstash folder
	- things will be added to various subfolders
	- we mainly need the contrib bundle to get
		- elapsed.rb logstash filter into logstash-1.4.2\lib\logstash\filters
		- jruby-win32ole into logstash-1.4.2\vendor\bundle\jruby\1.9\gems
- **Note:** this is a large installation of  ~130MB gzipped.  there are lighter agents to try.
- Logstash agent **must run as admin user** so it can consume windows event logs
	- configure as windows service with appropriate access
	- have the service restart if it crashes
	- crash logs will be dumped in the working director
- Assuming conf file and log files will be in the same dir as the software:
	- Set current/working dir to c:\temp\logstash-agent
	- Make sure `%JAVA_HOME%` is set
	- then run `bin\logstash agent -f logstash-winevent.conf -l agent.log`
	- that’s it!

##### Useful links and docs
- [http://logstash.net/docs/1.4.2/tutorials/10-minute-walkthrough/](http://logstash.net/docs/1.4.2/tutorials/10-minute-walkthrough/)
- [http://logstash.net/docs/1.4.2/learn](http://logstash.net/docs/1.4.2/learn)
- [http://www.elasticsearch.org/overview/elkdownloads/](http://www.elasticsearch.org/overview/elkdownloads/)
    