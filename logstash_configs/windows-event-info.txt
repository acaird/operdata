This file documents information relating to windows logins,
logouts, process creation and process termination.  It documents
the relevant event logs events generated under Windows as
well as how the logstash client processes those events.

Created by Jim Rennell 2014-12-05

This information is accurate for:
 - Windows events with a configdate of 2014-12-04 or later
 - Milestone 1 of sessiondata.rb (Windows only)



CAEN Logstash common event fields and tags
-----------------------------------------------

Common fields
  @timestamp
  username
  host
  type - Either "windows" or "linux"
  product - "RHEL7-CLSE" or "Win7-CLSE"
  edition - "Instruction" or "Research"
  version - "2014 R1"
  configdate - date on which the logstash configuration file was last changed

Common tags
  caen


CAEN Logstash process creation/termination event logging information
--------------------------------------------------------------------------

Process creation specific fields
  sessionid - A string unique to each user session
  securityid - Windows user id string (GUID)
  username
  accountdomain - domain to which the user account belongs
  logonid - Number unique to this user login session
  pid - id # of process
  processname - fq name of executable
  exe - fq name of executable
  comm - shortname of executable
  tokenelevationtype
  ppid - id # of parent process

Process termination specific fields
  sessionid - A string unique to each user session
  securityid - Windows user id string (GUID)
  username
  accountdomain - domain to which the user account belongs
  logonid - Number unique to this user login session
  pid - id # of process
  processname - fq name of executable
  exe - fq name of executable
  comm - shortname of executable
  exitstatus - exit status of process
  elapsed - elapsed clock time between process creation and termination in seconds

Process creation specific tags
  processCreation - signifies a process creation event

Process termination specific tags
  processTermination - signifies a process termination event
  elapsed.match - indicates that this event was matched to the creation event for this process instance


CAEN Logstash Login/Logout event logging information
-----------------------------------------------------------

Login specific fields
  sessionid - A string unique to each user session
  remotehost - A fqdn, an ip address or "localhost"

Logout specific fields
  sessionid - A string unique to each user session.  May be set to "unknown".
  remotehost - A fqdn, an ip address, "localhost" or "unknown"
  duration - Time of user session in minutes (Windows only)
  userstarttime - Timestamp from start of user session (Windows only).  May be set to "unknown".

Login specific tags
  userstart - Marks the start of a user session

Logout specific tags
  userend - Marks the end of a user session
  start_without_end - No matchng userstart event was observed.  


Comments: The CAEN-written sessiondata.rb filter plugin is used to manage
the fields relating to login/logout events.  It is also used to add a
sessionid field to application creation and termination events.  Some
fields may receive a value of "unknown" if no corresponding userstart
event was tracked.  The first event for each username with ID number
4624 is considered to be the login event beggining that user's session.
An event with ID number 4647 is considered to be logout event
ending the session of the user named in it.




===============================================================
Information on Windows generated login and logout events
---------------------------------------------------------------

This information is based on observations of Windows 7 Enterprise.

The following login-related events were observed in the Windows security event log:
  Event ID 4624 -- Logon
  Event ID 4634 -- Logoff
  Event ID 4647 -- "User-initiated logoff"
  Event ID 4648 -- "attempt to use explicit credentials" (meaning someone entered a password , inserted a smart card, etc.)

Process creation events (ID 4688) and process termination events (ID 4689) were also observed.

All of the events under observation carry the user's username.

Two different behaviors were observed with different user accounts.  The reason for the difference is unknown.
Because of this we regard all events with a given username to be part of the same session for data gathering purposes.
The events differ also for local logins versus remote logins.


Case 1A: Local login with behavior A
-------------------------------------
Event ID 4648
Event ID 4624 with logon type 2 (local) and LogonID (session ID number) X.  We regard this as the start of the user session.
...
Various application creation/termination events occur as the user launches and terminates programs and as the logon script
  launches and terminats programs.  The events have a LogonID of X.
...
Event ID 4647 with LoginID X.  We regard this  as the end of the user session.
Event ID 4634 with LoginID X.  This event can occur minutes or hours after the login is initiated and may not occur at all.


Case 1B: Local login with behavior B
-------------------------------------
Event ID 4648
Event ID 4624 with logon type 2 (local) and LogonID (session ID number) X and non-zero LogonGUID.
  We regard this as the start of the user session, though the following 4624 event could be used as well.
Event ID 4624 with logon type 2 (local) and LogonID (session ID number) Y and a zero-LogonGUID.
...
Various application creation/termination events occur as logon script launches and terminates programs.  The events have a LogonID of X.
Various application creation/termination events occur as the user launches and terminates programs.  The events have a LogonID of Y.
...
Event ID 4647 with LoginID Y.  We regard this as the end of the user session.
Event ID 4634 with LoginID Y.  This event can occur minutes or hours after the login is initiated.
Event ID 4634 with LoginID X.  This event can occur minutes or hours after the login is initiated.


Case 2A: Remote login with behavior A
-------------------------------------
Event ID 4648
Event ID 4624 with logon type 10 (remote) and LogonID (session ID number) X.  We regard this as the start of the user session.
...
Various application creation/termination events occur as the user launches and terminates programs and as the logon script
  launches and terminates programs.  The events have a LogonID of X.
...
Event ID 4647 with LoginID X.  We regard this as the end of the user session.
Event ID 4634 with LoginID X.  This event can occur minutes or hours after the login is initiated and may not occur at all.


Case 2B: Remote login with behavior B
-------------------------------------
Event ID 4624 with logon type 3 (network) and LogonID (session ID number) Z where Z is sometimes zero.
Event ID 4648
Event ID 4624 with logon type 10 (remote) and LogonID (session ID number) X and non-zero LogonGUID.
  We regard this as the start of the user session, though the following 4624 event could be used as well.
Event ID 4624 with logon type 10 (remote) and LogonID (session ID number) Y and a zero-LogonGUID.
...
Various application creation/termination events occur as the logon script launches and terminates programs.  The events have a LogonID of X.
Various application creation/termination events occur as the user launches and terminates programs.  The events have a LogonID of Y.
...
Event ID 4647 with LoginID Y.  We regard this as the end of the user session.
Event ID 4634 with LoginID Y.  This event can occur minutes or hours after the login is initiated.
Event ID 4634 with LoginID X.  This event can occur minutes or hours after the login is initiated.
Event ID 4634 with LoginID Z.  This event can occur minutes or hours after the login is initiated.


Case 3A: Console unlock with behavior A
---------------------------------------
Event ID 4648
Event ID 4624 with logon type 7 (unlock) and LogonID (session ID number) W.
Event ID 4634 with LoginID W.  This follows the 4624 event in quick succession.
 - Note that W is different from the login ID obtained at logon

Case 3B: Console unlock with behavior B
---------------------------------------
Event ID 4648
Event ID 4624 with logon type 7 (unlock) and LogonID (session ID number) W and non-zero LogonGUID.
Event ID 4624 with logon type 7 (unlock) and LogonID (session ID number) V and a zero-LogonGUID.
Event ID 4634 with LoginID V.  This follows the 4624 event in quick succession.
Event ID 4634 with LoginID W.  This follows the 4624 event in quick succession.
 - Note that W and V are different from the login IDs obtained at logon

