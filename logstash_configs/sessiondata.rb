# sessiondata filter
#
# Written by Jim Rennell (jrennell@umich.edu) 2014-11-20
#
# This filter tracks login session data and adds session-related
# fields and tags to events as appropriate.  The data that is tracked
# is a sessionkey, a sessionid, remotehost, session start time and
# session duration.  Sessions are delineated by an event with a
# start_tag and by an event with an end_tag.  The start_tag can be
# added to an event via the filter or by other means.  The end_tag
# must be added to an event by means other than the filter.  When
# an event which already has a start_tag or which is to receive a
# start_tag from the filter is filtered, the tracked data for that
# session is recorded and associated with the specified sessionkey.
# The specified sessioninfo parameter is recorded as the sessionid
# and that sessionid is added as a field of the event.
# When an event that already has an end_tag is filtered, the following
# fields are added to the event: duration, userstarttime, remotehost
# and sessionid.  The data for that session is then deleted.
# All other fields with a tracked sessionkey will have the sessionid
# field added to them
#
# Usage:
#
# filter {
#   sessiondata {
#     sessioninfo => host+username+timestamp	[required]
#     sessionkey => username	[required]
#     start_tag => "userstart"	[required]
#     end_tag => "userend"	[required]
#     add_start_tag => false	[optional - default: false]
#     remotehost => remotehost	[optional - default: "unknown"]
#   }
# }
#
#
# Added add_start_tag feature 2014-12-01
# Added support for remotehost and duration fields 2014-12-03
#

require "logstash/filters/base"
require "logstash/namespace"

# Define dictionary for holding session data
# Make it global to the whole program so that it persists and is accessible to all instances
$start_data = {}

class LogStash::Filters::Sessiondata< LogStash::Filters::Base

  # Define plugin name, milestone (version) and parameters
  config_name "sessiondata"
  milestone 1
  config :sessioninfo, :validate => :string, :required => true
  config :sessionkey, :validate => :string, :required => true
  config :start_tag, :validate => :string, :required => true
  config :end_tag, :validate => :string, :required => true
  config :add_start_tag, :validate => :boolean, :required => false, :default => false
  config :remotehost, :validata => :string, :required => false, :default => "unknown"

  public
  def register
@logger.info("Hello from sessiondata::register")	# Write to agent.log file
  end # def register

  public
  def filter(event)
@logger.info("Hello from sessiondata::filter")
#$start_data.each_pair do |k, e|
#  @logger.info("sessionids - k: #{k}, id: #{e.id}")
#end

    # return nothing unless there's an actual filter event
    return unless filter?(event)
#    return if sessionkey.nil?

    key = event.sprintf(@sessionkey)	# Note: event.sprintf replaces the variables referenced in the passed string with their values
    if(@add_start_tag)	# Add start tag to event if requested
      unless($start_data.has_key?(key))	# Only add start tag if we don't already have the key for this session
	event.tag(@start_tag)
      end
    end

    if(start_event?(event))	# Check if this is a start event.  If so, add session id to event and store session data as appropriate
      if($start_data.has_key?(key))	# Check if key exists
        event["sessionid"] = ($start_data[key]).id	#  This is a duplicate start event, so add existing sessionid to event
      else	# key is not in dictionary, so use new key and add session id and start time to dictionaries
        $start_data[key] = LogStash::Filters::Sessiondata::Element.new(event)	# create new start data (event) which automatically sets its @timestamp field
        sessionid = event.sprintf(@sessioninfo)	# Use sessioninfo as the session id for this key's session.
        event["sessionid"] = sessionid	# Add session id field to event
        ($start_data[key]).id = sessionid	# Store session id for later use
        ($start_data[key]).remotehost = event.sprintf(@remotehost)	# Store remotehost for later use
      end
    elsif(end_event?(event))	# Check if this is an end event.  If so add duration to event and remove sessionid and start time from dictionaries
      if($start_data.has_key?(key))	# Use existing data for this session
        event["sessionid"] = ($start_data[key]).id	# Add session id field to event
        event["duration"] = ((event["@timestamp"] - ($start_data[key]).event["@timestamp"]) / 60).to_i	# Add duration field to event
        event["userstarttime"] = ($start_data[key]).event["@timestamp"]	# Add userstarttime field to event
        event["remotehost"] = ($start_data[key]).remotehost	# Add remotehost field to event
        $start_data.delete(key)	# delete the data for this session as it is no longer needed
      else	# No start data exists for this event (Don't know if this would ever happen for an end event)
        event["sessionid"] = "unknown"	# Add session id field to event
        event["duration"] = 0	# Add duration field to event
        event["userstarttime"] = "unknown"	# Add userstarttime field to event
        event["remotehost"] = "unknown"	# Add remotehost field to event
	event.tag("end_without_start")	# Add a tag to event to indicate missing start data
      end
    elsif	# Handle an intermediate event (i.e. not tagged as start or end event)
      if($start_data.has_key?(key))	# Check if there is existing data for this session
        event["sessionid"] = ($start_data[key]).id	#  Add existing sessionid to event
      else	# No start data exists for this event (This happens with events that are not part of a tracked session)
        event["sessionid"] = "unknown"	# Add session id field to event
      end
    end # if(start_event?(event))

    # Indicate match
    filter_matched(event)	# Must make this call before returning with success

  end # end filter(event)


  private
  # Method to test for start events
  def start_event?(event)
    return (event["tags"] != nil && event["tags"].include?(@start_tag))
  end

  # Method to test for end events
  def end_event?(event)
    return (event["tags"] != nil && event["tags"].include?(@end_tag))
  end

end # class LogStash::Filters::Addsessionid


# This is used to instantiate new instances of start data
class LogStash::Filters::Sessiondata::Element
  attr_accessor :event, :id, :remotehost

  def initialize(event)
    @event = event	# We declare an event here for its @timestamp field which gets created at instantiation
    @id = "unknown"
    @remotehost = "unknown"
  end
end
