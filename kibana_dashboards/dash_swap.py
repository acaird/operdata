"""Copy named dashboard from one kibana instance to another.
Alter the dashboard 'loader' definition to disable loads and saves.

First tries to connect via locahost port-forwarding, then
tries to connect to wapiti.engin.umich.edu/es over SSL,
which will only work if you are on the 141.213.40-net."""

import sys
from elasticsearch import Elasticsearch
from pprint import pprint as pp
import json
import os

# To test if localhost:9200 is listening.
import socket
import errno

ES_HOSTS = ['localhost',
            'hpc-es.engin.umich.edu',
            'wapiti.engin.umich.edu']

# Only here to suppress InsecureRequestWarning when connecting to
# elasticsearch over SSL, via es = Elasticsearch(...) def below.
# Keeps command-line less noisy, but not the greatest, safest idea.
import urllib3
urllib3.disable_warnings()

# Better would be to capture warnings to our own log...
#import logging
#logging.captureWarnings(True)

# Better still would be to specify a cacert bundle that _did_ allow
# python and urllib3 to verify U-M's InCommon-issued certs.
# Tried with certifi and certifi.where() but did not work...
#import certifi

def main(argv):

    if(len(sys.argv) != 2):
        print 'USAGE: Supply name of dashboard to copy.'
        exit(1)

    # Check to see if port forwarding appears to be working.
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect(('localhost', 9200))
        s.shutdown(2)#disconnect
        print "Connecting to {}:9200...\n".format(ES_HOSTS[0])
        # Connect to ES via localhost with port forwarding
        es = Elasticsearch(host=ES_HOSTS[0])
    except:
        print "Connecting to {}/es...\n".format(ES_HOSTS[2])
        #sys.exit(errno.EPERM)
        # Connect to the ES instance as if you are on the 40-net
        es = Elasticsearch(
                    host=ES_HOSTS[2],
                    url_prefix='es',
                    port=443,
                    use_ssl=True,
                    verify_certs=False,       # does not work
                    #ca_certs=os.path.abspath('wapiti_bundle.pem')    # does not work
                    #ca_certs=certifi.where()    # does not work
                    )

    # The dashboard title _is_ the id we need to search on
    dash_title = index = argv[1]

    # Search the source kibana-int dashboards.
    records = es.search(index='kibana-int',
                        q='_type:dashboard AND _id:'+dash_title)

    # Assume there is one, only one, and always one.
    if records['hits']['total'] <> 1:
        print "No dashboard found with that title."
        exit(1)

    dashdoc = records['hits']['hits'][0]['_source']

    # The dashdoc has the entire _source in it, but the actual
    # dashboard definition is an escaped JSON string under the
    # key 'dashboard'.
    # Get it and make it a python object via json.loads
    dash = json.loads(dashdoc['dashboard'])

    # Update certain loader keys to make the dashboard less saveable
    # and to prevent 'local' loading in 'view only' kibana instance.
    dash['loader']['save_gist'] = True
    dash['loader']['save_elasticsearch'] = False
    dash['loader']['save_default'] = False
    dash['loader']['load_local'] = False
    #pp(dash['loader'])

    # Repack the updated dashboard definition into a string
    newdash = json.dumps(dash)

    # Replace the old dashdoc['dashboard'] key with our updated
    # and re-json-ified version.
    dashdoc['dashboard'] = newdash

    # In the view-only kibana instance (it has its own es index), we
    # want to brutally replace as we copy.
    # if a doc with the given id (e.g. dashboard name) exists, toss it.

    # Out with the old! (Don't care of it succeeds or not.)
    es.delete(index='caen-view-int',
            doc_type="dashboard",
            id=dash_title,
            ignore=[404])

    # In with the new! (Just assuming it succeeds?)
    es.index(index='caen-view-int',
            doc_type="dashboard",
            id=dash_title,
            body=dashdoc)

    print "Swapped dashboard '%s' from working kibana to view-only kibana." % (dash_title)
    print "open https://wapiti.engin.umich.edu/caen-view/#/dashboard/elasticsearch/%s" % (dash_title)

    exit(0)

if __name__ == '__main__':
    main(sys.argv)
