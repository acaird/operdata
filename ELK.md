# ELK Stack

The ELK (Elasticsearch, Logstash, and Kibana) are used to collect, store, and data mine log and document data.  HPC data, primarily in the form of logs.  CAEN is also using it to monitor software utilization in the labs.  This environment is **not** to be given to the end users, as it is not secure yet.

msbritt gave a [talk at MoabCon2014](http://www.youtube.com/watch?v=JRvZk9Jk54M&index=28&list=PLkn6nySofDwtZwNCQBrFDclyfT1yeYCCw)  which gives an overview of capturing HPC logs into the ELK stack.

## Logstash - HPC
We have logstash daemons which monitor:
* syslog
* pbs_mom logs
* pbs_server logs
* moab logs
* moab accounting logs
* globus / gridftp logs
* auditd logs

These daemons monitor logfiles, use regular expressions to grab specific lines, parse and munge the data to extract or identify particular information (like a job id or a user id), and then send them on to a central server (redis).

Logstash is also used as in indexer to pull data from redis and send the data to the correct Elasticsearch index.

## Redis

Redis is a key-value store, running centrally on a Flux admin machine (currently flux-admin03.engin.umich.edu).   We push all logstash events from the compute, login, and head nodes to Redis, then use another logstash instance to pull the events from the stack and put into an appropriate elasticsearch index.

## Elasticsearch

Elasticsearch is a scalable, Apache Lucene-based schema-less document store which provides great search capability.  

To access (and you will need this for access to Kibana), port-forward port 9200 from your local machine into Flux.   

Using .ssh/config 

		Host flux
			Hostname flux-xfer.engin.umich.edu
			LocalForward 9200 hpc-es.engin.umich.edu:9200
				
Then `ssh flux`

or 

		ssh -N -L 9200:hpc-es.engin.umich.edu:9200 flux-xfer


## Kibana

Kibana is a javascript-based web front end to display and help query data in Elasticsearch indexes.  It allows your web browser to talk directly with the Elasticsearch instance, so you must have the port-forwarding complete to use Kibana with our instance.

Point your browser to [flux-kibana.engin.umich.edu/kibana](http://flux-kibana.engin.umich.edu/kibana) to see the dashboards. 

To access the different dashboards, use the `Load` option in the top right window. To create a new dashboard, you can start from scratch or load the dashboard closest to what you want and then `Save` it as a different name.  You'll see the name change in the top left of the window.  Do **not** click the 'x' when looking at the dashboards unless you're dealing with your own dashboards.  This will delete the dashboard forever from the Elasticsearch index.  People will be unhappy.
